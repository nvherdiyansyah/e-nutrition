﻿using Owin;
using Microsoft.Owin;
[assembly: OwinStartup(typeof(E_Nutriton.Startup))]
namespace E_Nutriton
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Any connection or hub wire up and configuration should go here
            app.MapSignalR();
        }
    }
}