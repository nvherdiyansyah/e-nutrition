﻿using E_Nutriton.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Nutriton.Controllers
{
    public class ErrorLogController : Controller
    {
        db_enutritionEntities dbModel = new db_enutritionEntities();

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetData()
        {
            using (db_enutritionEntities dbModel = new db_enutritionEntities())
            {
                List<tbl_error_log> errorList = dbModel.tbl_error_log.ToList<tbl_error_log>();
                List<ErrorOutput> result = new List<ErrorOutput>();
                foreach (var item in errorList)
                {

                    result.Add(new ErrorOutput
                    {
                        id_error = item.id_error,
                        error_line = item.error_line,
                        performed_by = item.performed_by,
                        error_time = item.error_time.ToString(),
                        error_description = item.error_description
                    });
                }

                return Json(new { data = result.OrderByDescending(e => e.id_error) }, JsonRequestBehavior.AllowGet);
            }
        }

        public class ErrorOutput
        {
            public int id_error { get; set; }
            public string error_line { get; set; }
            public string performed_by { get; set; }
            public string error_time { get; set; }
            public string error_description { get; set; }
        }
    }
}