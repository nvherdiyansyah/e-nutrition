﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using E_Nutriton.Models;

namespace E_Nutriton.Controllers
{
    public class DashboardNutritionistController : Controller
    {
        // GET: DashboardNutritionist
        db_enutritionEntities dbModel = new db_enutritionEntities();
        [Authorize]
        public ActionResult Index()
        {
            if (Session["LoginNutritionist"] != null)
            {

                int IDNutritionist = Convert.ToInt32(Session["IdNutritionist"]);
                var user = dbModel.tbl_patient.Where(a => a.id_nutritionist == IDNutritionist && a.is_account_active == true).ToList();
                ViewBag.CountPatient = user.Count().ToString();
                var userregis = dbModel.tbl_patient.Where(a => a.id_nutritionist == IDNutritionist && a.is_account_active == false && a.certificate != "~/img/certificate/default-certificate.jpg").ToList();
                ViewBag.CountPatientRegis = userregis.Count().ToString();
                return View();
            }
            else
            {
                return RedirectToAction("Login", "User");
            }
        }

        [HttpGet]
        [Authorize]
        public ActionResult MyProfile()
        {
            if (Session["LoginNutritionist"] != null)
            {
                int IDNutritionist = Convert.ToInt32(Session["IdNutritionist"]);
                ViewBag.Message = TempData["Message"];
                ViewBag.Status = TempData["Status"];
                var user = dbModel.tbl_nutritionist.Where(c => c.id_nutritionist == IDNutritionist).FirstOrDefault();
                return View(user);
            }
            else
            {
                return RedirectToAction("Login", "User");
            }
        }

        [HttpPost]
        public ActionResult UpdateProfile(tbl_nutritionist UserUpdate)
        {
            using (var transaction = dbModel.Database.BeginTransaction())
            {
                try
                {
                    // Make path for image file
                    //var cek = UserUpdate.id_nutritionist;
                    //var cek2 = Session["IdNutritionist"];
                    var message = "";
                    var status = false;
                    var user = dbModel.tbl_nutritionist.Where(a => a.id_nutritionist == UserUpdate.id_nutritionist).FirstOrDefault();
                    if (user != null)
                    {
                        if (UserUpdate.ImagePictureFile != null)
                        {
                            string fileNamePicture = Path.GetFileNameWithoutExtension(UserUpdate.ImagePictureFile.FileName);
                            string extensionPicture = Path.GetExtension(UserUpdate.ImagePictureFile.FileName);
                            fileNamePicture = fileNamePicture + DateTime.Now.ToString("yymmssfff") + extensionPicture;
                            UserUpdate.picture = "~/img/picture/" + fileNamePicture;
                            fileNamePicture = Path.Combine(Server.MapPath("~/img/picture/"), fileNamePicture);
                            UserUpdate.ImagePictureFile.SaveAs(fileNamePicture);
                            user.picture = UserUpdate.picture;
                        }

                        if (UserUpdate.ImageCertificateFile != null)
                        {
                            string fileNameCertificate = Path.GetFileNameWithoutExtension(UserUpdate.ImageCertificateFile.FileName);
                            string extensionCertificate = Path.GetExtension(UserUpdate.ImageCertificateFile.FileName);
                            fileNameCertificate = fileNameCertificate + DateTime.Now.ToString("yymmssfff") + extensionCertificate;
                            UserUpdate.certificate = "~/img/certificate/" + fileNameCertificate;
                            fileNameCertificate = Path.Combine(Server.MapPath("~/img/certificate/"), fileNameCertificate);
                            UserUpdate.ImageCertificateFile.SaveAs(fileNameCertificate);
                            user.certificate = UserUpdate.certificate;
                        }

                        user.first_name = UserUpdate.first_name;
                        user.last_name = UserUpdate.last_name;
                        user.gender = UserUpdate.gender;

                        if (UserUpdate.address != null)
                        {
                            user.address = UserUpdate.address;
                        }

                        user.date_birth = UserUpdate.date_birth;
                        user.description = UserUpdate.description;


                        dbModel.Entry(user).State = EntityState.Modified;
                        dbModel.Configuration.ValidateOnSaveEnabled = false;
                        dbModel.SaveChanges();
                        SessionUpdate(user);
                        ModelState.Clear();                        
                        message = "Profile has been updated";
                        status = true;
                    }
                    else
                    {
                        status = false;
                        message = "Account not found";
                    }

                    TempData["Message"] = message;
                    TempData["Status"] = status;
                    transaction.Commit();
                    return RedirectToAction("MyProfile", "DashboardNutritionist");
                }
                catch (Exception msgError)
                {
                    transaction.Rollback();
                    TempData["Message"] = msgError.Message;
                    TempData["Status"] = false;
                    return RedirectToAction("MyProfile", "DashboardNutritionist");
                }
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdatePassword(tbl_nutritionist UserUpdate)
        {
            using (var transaction = dbModel.Database.BeginTransaction())
            {
                try
                {
                    //var cek3 = UserUpdate.PasswordUpdate.id_nutritionist;
                    //var cek2 = Session["IdNutritionist"];
                    var message = "";
                    var status = false;
                    var user = dbModel.tbl_nutritionist.Where(a => a.id_nutritionist == UserUpdate.PasswordUpdate.id_nutritionist).FirstOrDefault();
                    if (user != null)
                    {
                        if (user.password == Crypto.Hash(UserUpdate.PasswordUpdate.current_password))
                        {
                            if (UserUpdate.PasswordUpdate.new_password == UserUpdate.PasswordUpdate.confirm_password)
                            {
                                user.password = Crypto.Hash(UserUpdate.PasswordUpdate.new_password);
                                dbModel.Configuration.ValidateOnSaveEnabled = false;
                                dbModel.SaveChanges();
                                status = true;
                                message = "Password change successfully";
                            }
                            else
                            {
                                status = false;
                                message = "New password and confirm password not match";
                            }
                        }
                        else
                        {
                            status = false;
                            message = "Password Incorrect";
                        }
                    }
                    else
                    {
                        status = false;
                        message = "Invalid Request";
                    }


                    TempData["Message"] = message;
                    TempData["Status"] = status;
                    transaction.Commit();

                    /*
                    if (ModelState.IsValid)
                    {
                        var user = dbModel.tbl_nutritionist.Where(a => a.id_nutritionist == UserUpdate.PasswordUpdate.id_nutritionist).FirstOrDefault();
                        if (user != null)
                        {
                            user.password = Crypto.Hash(UserUpdate.PasswordUpdate.new_password);
                            dbModel.Configuration.ValidateOnSaveEnabled = false;
                            dbModel.SaveChanges();
                            message = "Password change successfully";
                        }
                    }
                    else
                    {
                        message = "Invalid Request";
                    }

                    
                    var user = dbModel.tbl_nutritionist.Where(a => a.id_nutritionist == UserUpdate.PasswordUpdate.id_nutritionist).FirstOrDefault();
                    if (user != null)
                    {
                        if (user.password != UserUpdate.PasswordUpdate.current_password)
                        {
                            message = "Current password wrong";
                        }
                        else if (UserUpdate.PasswordUpdate.new_password != UserUpdate.PasswordUpdate.confirm_password)
                        {
                            message = "New password and confirm password not match";
                        }
                        else
                        {
                            user.password = Crypto.Hash(UserUpdate.PasswordUpdate.new_password);
                            dbModel.Configuration.ValidateOnSaveEnabled = false;
                            dbModel.SaveChanges();
                            message = "Password change successfully";
                        }                        
                    }
                    else
                    {
                        message = "Account not Found";
                    }
                    */
                    
                    return RedirectToAction("MyProfile", "DashboardNutritionist");
                }
                catch (Exception msgError)
                {
                    transaction.Rollback();
                    TempData["Message"] = msgError.Message;
                    TempData["Status"] = false;
                    return RedirectToAction("MyProfile", "DashboardNutritionist");
                } 
            }
        }

        private void SessionUpdate(tbl_nutritionist user)
        {
            Session["IdNutritionist"] = user.id_nutritionist.ToString();
            Session["FirstName"] = user.first_name.ToString();
            Session["LastName"] = user.last_name.ToString();
            Session["Picture"] = user.picture.ToString();
        }
    }
}