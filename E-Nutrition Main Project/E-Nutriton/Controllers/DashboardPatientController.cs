﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using E_Nutriton.Models;

namespace E_Nutriton.Controllers
{
    public class DashboardPatientController : Controller
    {
        // GET: DashboardPatient
        db_enutritionEntities dbModel = new db_enutritionEntities();

        
        [Authorize]
        public ActionResult Index()
        {
            if (Session["LoginPatient"] != null)
            {
                int IDPatint = Convert.ToInt32(Session["Idpatient"]);
                List<tbl_daily> dailysearch = dbModel.tbl_daily.Where(b => b.id_pantient == IDPatint).ToList<tbl_daily>();
                int TotEnergyAll = 0;
                foreach (var item in dailysearch)
                {
                    TotEnergyAll = TotEnergyAll + Convert.ToInt32(item.total_energy_daily);
                }

                int TotDaily = dailysearch.Count();

                ViewBag.TotEnergyAll = TotEnergyAll;
                ViewBag.TotDaily = TotDaily;
                var user = dbModel.tbl_patient.Where(a => a.is_account_active == true).ToList();

                //untuk article 
                var content = dbModel.tbl_article.Select(s => new
                {
                    s.Id_article,
                    s.Title,
                    s.Picture,
                    s.Content,
                    s.Release_date

                });

                List<ArticleViewModel> contentModel = content.Select(item => new ArticleViewModel()
                {
                    Id_article = item.Id_article,
                    Title = item.Title,
                    Picture = item.Picture,
                    Content = item.Content,
                    Release_date = item.Release_date

                }).ToList();
                return View(contentModel);
            }
            else
            {
                return RedirectToAction("Login", "Patient");
            }
        }
        //untuk article 
        public ActionResult RetrieveImage(int id)
        {
            byte[] cover = GetImageFromDataBase(id);
            if (cover != null)
            {
                return File(cover, "image/jpg");
            }
            else
            {
                return null;
            }
        }

        //untuk article 
        public byte[] GetImageFromDataBase(int Id)
        {
            var q = from temp in dbModel.tbl_article where temp.Id_article == Id select temp.Picture;
            byte[] cover = q.First();
            return cover;
        }

        //untuk article 
        // GET: Customer/Details/5
        public ActionResult DetailArticle(int id)
        {
            return View(dbModel.tbl_article.Where(b => b.Id_article == id).FirstOrDefault());            
        }

        [HttpGet]
        [Authorize]
        public ActionResult MyProfile()
        {
            int IDPatient = Convert.ToInt32(Session["Idpatient"]);
            ViewBag.Message = TempData["Message"];
            ViewBag.Status = TempData["Status"];
            var user = dbModel.tbl_patient.Where(c => c.id_pantient == IDPatient).FirstOrDefault();            
            return View(user);

        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateProfile(tbl_patient UserUpdate)
        {
            using (var transaction = dbModel.Database.BeginTransaction())
            {
                try
                {

                   
                    // Make path for image file
                    //var cek = UserUpdate.id_pantient;
                    //var cek2 = Session["Idpatient"];
                    var message = "";
                    var status = false;
                    if (UserUpdate.height == null)
                    {
                        message = "Height Cannot be null";
                        TempData["Message"] = message;
                        TempData["Status"] = status;
                        return RedirectToAction("MyProfile");
                    
                    }

                    if (UserUpdate.weight == null)
                    {
                        message = "Weight Cannot be null";
                        TempData["Message"] = message;
                        TempData["Status"] = status;
                        return RedirectToAction("MyProfile");

                    }
                    var user = dbModel.tbl_patient.Where(a => a.id_pantient == UserUpdate.id_pantient).FirstOrDefault();
                    if (user != null)
                    {
                        if (UserUpdate.ImagePictureFile != null)
                        {
                            string fileNamePicture = Path.GetFileNameWithoutExtension(UserUpdate.ImagePictureFile.FileName);
                            string extensionPicture = Path.GetExtension(UserUpdate.ImagePictureFile.FileName);
                            fileNamePicture = fileNamePicture + DateTime.Now.ToString("yymmssfff") + extensionPicture;
                            UserUpdate.picture = "~/img/picture/" + fileNamePicture;
                            fileNamePicture = Path.Combine(Server.MapPath("~/img/picture/"), fileNamePicture);
                            UserUpdate.ImagePictureFile.SaveAs(fileNamePicture);
                            user.picture = UserUpdate.picture;
                        }

                        if (UserUpdate.ImageLetterFile != null)
                        {
                            string fileNameLetter = Path.GetFileNameWithoutExtension(UserUpdate.ImageLetterFile.FileName);
                            string extensionLetter = Path.GetExtension(UserUpdate.ImageLetterFile.FileName);
                            fileNameLetter = fileNameLetter + DateTime.Now.ToString("yymmssfff") + extensionLetter;
                            UserUpdate.certificate = "~/img/letter/" + fileNameLetter;
                            fileNameLetter = Path.Combine(Server.MapPath("~/img/letter/"), fileNameLetter);
                            UserUpdate.ImageLetterFile.SaveAs(fileNameLetter);
                            user.certificate = UserUpdate.certificate;
                        }

                        user.first_name = UserUpdate.first_name;
                        user.last_name = UserUpdate.last_name;
                        user.gender = UserUpdate.gender;
                        user.height = UserUpdate.height;
                        user.weight = UserUpdate.weight;
                        user.symptoms = UserUpdate.symptoms;
                        user.date_birth = UserUpdate.date_birth;
                        string GenderTemp = UserUpdate.gender;
                        int HeightTemp = Convert.ToInt32(UserUpdate.height);
                        int WeightTemp = Convert.ToInt32(UserUpdate.weight);
                        int IdealTemp = 0;
                        if (GenderTemp  == "Male")
                        {                            
                            IdealTemp = (HeightTemp - 100) - (HeightTemp - 100) * 10 / 100;
                        }
                        else if (GenderTemp == "Female")
                        {
                            IdealTemp = (HeightTemp - 100) - (HeightTemp - 100) * 15 / 100;
                        }

                        int margin = 0;
                        string conditiontemp = "";
                        if (IdealTemp>WeightTemp)
                        {
                            margin = IdealTemp - WeightTemp;
                            
                        }
                        else
                        {
                            margin = WeightTemp - IdealTemp;
                        }

                        if (0 <= margin && margin < 3)
                        {
                            conditiontemp = "Ideal";
                        }
                        else if (3 <= margin && margin < 5)
                        {
                            conditiontemp = "Almost Ideal";
                        }
                        else if (5 <= margin && margin < 7)
                        {
                            conditiontemp = "Less Ideal";
                        }
                        else
                        {
                            conditiontemp = "Not Ideal";
                        }

                        user.ideal_weight = IdealTemp;
                        user.condition = conditiontemp;
                        
                        if (UserUpdate.address != null)
                        {
                            user.address = UserUpdate.address;
                        }
                        
                        //user.date_birth = ProfileUpdatePatientModel.date_birth;

                        dbModel.Entry(user).State = EntityState.Modified;
                        dbModel.Configuration.ValidateOnSaveEnabled = false;
                        dbModel.SaveChanges();
                        SessionUpdate(user);
                        ModelState.Clear();
                        message = "Profile has been updated";
                        status = true;
                    }
                    else
                    {
                        status = false;
                        message = "Account not found";
                    }


                    TempData["Message"] = message;
                    TempData["Status"] = status;
                    transaction.Commit();
                    
                    return RedirectToAction("MyProfile", "Dashboardpatient");
                }
                catch (Exception msgError)
                {
                    transaction.Rollback();
                    TempData["Message"] = msgError.Message;
                    TempData["Status"] = false;
                  
                    return RedirectToAction("MyProfile", "Dashboardpatient");
                }
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdatePassword(tbl_patient UserUpdate)
        {
            using (var transaction = dbModel.Database.BeginTransaction())
            {
                try
                {
                    //var cek3 = UserUpdate.PasswordUpdate.id_nutritionist;
                    //var cek2 = Session["IdNutritionist"];
                    var message = "";
                    var status = false;
                    var user = dbModel.tbl_patient.Where(a => a.id_pantient == UserUpdate.PasswordUpdatePatient.id_patient).FirstOrDefault();
                    if (user != null)
                    {
                        if (user.password == Crypto.Hash(UserUpdate.PasswordUpdatePatient.current_password))
                        {
                            if (UserUpdate.PasswordUpdatePatient.new_password == UserUpdate.PasswordUpdatePatient.confirm_password)
                            {
                                user.password = Crypto.Hash(UserUpdate.PasswordUpdatePatient.new_password);
                                dbModel.Configuration.ValidateOnSaveEnabled = false;
                                dbModel.SaveChanges();
                                status = true;
                                message = "Password change successfully";
                            }
                            else
                            {
                                status = false;
                                message = "New password and confirm password not match";
                            }
                        }
                        else
                        {
                            status = false;
                            message = "Password Incorrect";
                        }
                    }
                    else
                    {
                        status = false;
                        message = "Invalid Request";
                    }
                    

                    TempData["Message"] = message;
                    TempData["Status"] = status;
                    transaction.Commit();
                    return RedirectToAction("MyProfile", "DashboardPatient");
                }
                catch (Exception msgError)
                {
                    transaction.Rollback();
                    TempData["Message"] = msgError.Message;
                    TempData["Status"] = false;
                    return RedirectToAction("MyProfile", "DashboardPatient");
                }
            }
        }

        //tampilkan list nutritionist
        public static List<Models.tbl_nutritionist> selectnutritionist = new List<Models.tbl_nutritionist>();
        [HttpGet]
        [ValidateAntiForgeryToken]
        public ActionResult SelectNutritionist()
        {
            using (var context = new db_enutritionEntities())
            {

                // Return the list of data from the database 
                var data = context.tbl_nutritionist.ToList();
                return View(data);
            }
        }




        private void SessionUpdate(tbl_patient user)
        {
            Session["Idpatient"] = user.id_pantient.ToString();
            Session["FirstName"] = user.first_name.ToString();
            Session["LastName"] = user.last_name.ToString();
            Session["Picture"] = user.picture.ToString();
            Session["Height"] = user.height.ToString();
            Session["Weight"] = user.weight.ToString();
        }

    }
}