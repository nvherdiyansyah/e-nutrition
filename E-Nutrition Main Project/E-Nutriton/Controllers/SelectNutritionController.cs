﻿using E_Nutriton.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace E_Nutriton.Controllers
{
    public class SelectNutritionController : Controller
    {
        db_enutritionEntities dbModel = new db_enutritionEntities();
        public static List<Models.tbl_nutritionist> ListProfile = new List<Models.tbl_nutritionist>();
        [HttpGet]
        // GET: SelectNutrition
        public ActionResult Index()
        {
            using (var context = new db_enutritionEntities())
            {

                // Return the list of data from the database 
                var data = context.tbl_nutritionist.Where(a => a.is_account_active == true).ToList();
                return View(data);
            }
        }

        // POST: SelectNutrition/Edit/5
        [HttpPost]
        public ActionResult Select(int id)
        {
            try
            {
                // TODO: Add update logic here
                int IdPatient = Convert.ToInt32(Session["IdPatient"]);
                string message = "";
                var user = dbModel.tbl_patient.Where(x => x.id_pantient == IdPatient).FirstOrDefault();
                if (user != null)
                {
                    dbModel.Configuration.ValidateOnSaveEnabled = false; // this line to avoid confirm password does not
                    user.id_nutritionist = id;
                    dbModel.SaveChanges();
                }
                else
                {
                    message = "User Not Found";
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        [HttpGet]
        public ActionResult Select(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            db_enutritionEntities dbModel = new db_enutritionEntities();
            if (dbModel == null)
            {
                return HttpNotFound();
            }
            var user = dbModel.tbl_nutritionist.Where(a => a.id_nutritionist == id).FirstOrDefault();
            return View(user);
        }
    }
}