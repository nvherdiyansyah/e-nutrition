﻿using E_Nutriton.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Nutriton.Controllers
{
    public class AuditController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetData()
        {
            using (db_enutritionEntities dbModel = new db_enutritionEntities())
            {
                List<tbl_audit_trail> auditList = dbModel.tbl_audit_trail.ToList<tbl_audit_trail>();
                List<AuditOutput> result = new List<AuditOutput>();
                foreach (var item in auditList)
                {

                    result.Add(new AuditOutput
                    {
                        id_audit = item.id_audit,
                        table_name = item.table_name,
                        operation = item.operation,
                        occured_at = item.occured_at.ToString(),
                        performed_by = item.performed_by,
                        description = item.description
                    });
                }

                return Json(new { data = result.OrderByDescending(e => e.id_audit) }, JsonRequestBehavior.AllowGet);
            }
        }

        public class AuditOutput
        {
            public int id_audit { get; set; }
            public string table_name { get; set; }
            public string operation { get; set; }
            public string occured_at { get; set; }
            public string performed_by { get; set; }
            public string description { get; set; }
        }
    }
}