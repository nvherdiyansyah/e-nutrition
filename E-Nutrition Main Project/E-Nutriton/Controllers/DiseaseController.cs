﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using E_Nutriton.Models;
using System.Data.Entity;

namespace E_Nutriton.Controllers
{
    public class DiseaseController : Controller
    {
        // GET: Disease
        db_enutritionEntities dbModel = new db_enutritionEntities();

        public ActionResult Index()
        {
            return View();
        }
        
        [HttpGet]
        public JsonResult GetData()
        {
            using (db_enutritionEntities dbModel = new db_enutritionEntities())
            {
                //List<tbl_disease> diseaseList = dbModel.tbl_disease.ToList<tbl_disease>();
                //return Json(new { data = diseaseList }, JsonRequestBehavior.AllowGet);
                List<tbl_disease> diseaseList = dbModel.tbl_disease.ToList<tbl_disease>();
                List<DiseaseOutput> result = new List<DiseaseOutput>();
                foreach (var item in diseaseList)
                {

                    result.Add(new DiseaseOutput
                    {
                        id_disease = item.id_disease,
                        name_disease = item.name_disease,
                        energy = Convert.ToInt32(item.energy)
                    });
                }

                return Json(new { data = result.OrderByDescending(e => e.id_disease) }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult AddorEdit(int id = 0)
        {
            if (id == 0)
                return View(new tbl_disease());
            else
            {
                using (db_enutritionEntities dbModel = new db_enutritionEntities())
                {
                    return View(dbModel.tbl_disease.Where(x => x.id_disease == id).FirstOrDefault<tbl_disease>());
                }
            }
        }

        [HttpPost]
        public ActionResult AddorEdit(tbl_disease disease)
        {
            using (var transaction = dbModel.Database.BeginTransaction())
            {
                try
                {
                    if (disease.id_disease == 0)
                    {
                        dbModel.tbl_disease.Add(disease);
                        tbl_audit_trail audit = new tbl_audit_trail
                        {
                            table_name = "tbl_disease",
                            operation = "ADD",
                            occured_at = DateTime.Now,
                            performed_by = Session["Username"].ToString(),
                            description = "Add new Disease" + disease.name_disease
                        };
                        dbModel.tbl_audit_trail.Add(audit);

                        dbModel.SaveChanges();
                        transaction.Commit();
                        return Json(new { success = true, message = "Saved Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        dbModel.Entry(disease).State = EntityState.Modified;
                        tbl_audit_trail audit = new tbl_audit_trail
                        {
                            table_name = "tbl_disease",
                            operation = "EDIT",
                            occured_at = DateTime.Now,
                            performed_by = Session["Username"].ToString(),
                            description = "Edit Disease " + disease.name_disease
                        };
                        dbModel.tbl_audit_trail.Add(audit);

                        dbModel.SaveChanges();
                        transaction.Commit();
                        return Json(new { success = true, message = "Update Successfully" }, JsonRequestBehavior.AllowGet);

                    }
                }
                catch (Exception msgError)
                {
                    db_enutritionEntities dbModel2 = new db_enutritionEntities();

                    tbl_error_log errlog = new tbl_error_log()
                    {
                        error_line = msgError.StackTrace,
                        error_time = DateTime.Now,
                        performed_by = Session["Username"].ToString(),
                        error_description = msgError.Message
                    };
                    dbModel2.tbl_error_log.Add(errlog);
                    dbModel2.SaveChanges();
                    transaction.Rollback();
                    return Json(new { success = true, message = "Error " + msgError.Message }, JsonRequestBehavior.AllowGet);
                } 
            }

        }

        public ActionResult Delete(int id)
        {
            using (db_enutritionEntities dbModel = new db_enutritionEntities())
            {
                tbl_disease disease = dbModel.tbl_disease.Where(x => x.id_disease == id).FirstOrDefault<tbl_disease>();
                dbModel.tbl_disease.Remove(disease);
                tbl_audit_trail audit = new tbl_audit_trail
                {
                    table_name = "tbl_disease",
                    operation = "DELETE",
                    occured_at = DateTime.Now,
                    performed_by = Session["Username"].ToString(),
                    description = "Delete Disease " + disease.name_disease
                };
                dbModel.tbl_audit_trail.Add(audit);

                dbModel.SaveChanges();
                return Json(new { success = true, message = "delete Successfully" }, JsonRequestBehavior.AllowGet);

            }

        }

        public class DiseaseOutput
        {
            public int id_disease { get; set; }
            public string name_disease { get; set; }
            public int energy { get; set; }
        }

    }
}