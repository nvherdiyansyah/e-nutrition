﻿using E_Nutriton.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Nutriton.Controllers
{
    public class MealController : Controller
    {
        db_enutritionEntities dbModel = new db_enutritionEntities();
        // GET: Meal
        public ActionResult Breakfast(tbl_meal Meal)
        {
            string message = "";
            string dailyID = Session["IdDaily"].ToString();
            if (Session["IdDaily"] != null)
            {
                //int dailyID = Convert.ToInt32(Session["IdDaily"]);                
                var search = dbModel.tbl_daily.Where(a => a.id_hashing == dailyID).FirstOrDefault();
                var user = dbModel.tbl_meal.Where(x => x.id_daily == search.id_daily && x.meal_type == "Breakfast").FirstOrDefault();
                if (user != null)
                {
                    Session["IdMeal"] = user.id_meal;
                    return View(user);
                }
                else
                {
                    //Meal.id_daily = Convert.ToInt32(Session["IdDaily"]);
                    var search2 = dbModel.tbl_daily.Where(a => a.id_hashing == dailyID).FirstOrDefault();
                    Meal.id_daily = search2.id_daily;
                    Meal.meal_type = "Breakfast";
                    dbModel.tbl_meal.Add(Meal);
                    tbl_audit_trail audit = new tbl_audit_trail
                    {
                        table_name = "tbl_meal",
                        operation = "ADD",
                        occured_at = DateTime.Now,
                        performed_by = Session["FirstName"].ToString() + " " + Session["LastName"],
                        description = "Add " + Meal.meal_type
                    };
                    dbModel.tbl_audit_trail.Add(audit);
                    dbModel.SaveChanges();
                    var user2 = dbModel.tbl_meal.Where(x => x.id_daily == Meal.id_daily && x.meal_type == Meal.meal_type).FirstOrDefault();
                    Session["IdMeal"] = user2.id_meal;
                    return View(user2);
                }
                
            }
            else
            {
                message = "Invalid Request";
                ViewBag.Message = message;
                return View();
            }
            

        }

        public ActionResult Lunch(tbl_meal Meal)
        {
            string message = "";
            string dailyID = Session["IdDaily"].ToString();
            if (Session["IdDaily"] != null)
            {
                //int dailyID = Convert.ToInt32(Session["IdDaily"]);                
                var search = dbModel.tbl_daily.Where(a => a.id_hashing == dailyID).FirstOrDefault();
                var user = dbModel.tbl_meal.Where(x => x.id_daily == search.id_daily && x.meal_type == "Lunch").FirstOrDefault();
                if (user != null)
                {
                    Session["IdMeal"] = user.id_meal;
                    return View(user);
                }
                else
                {
                    //Meal.id_daily = Convert.ToInt32(Session["IdDaily"]);
                    var search2 = dbModel.tbl_daily.Where(a => a.id_hashing == dailyID).FirstOrDefault();
                    Meal.id_daily = search2.id_daily;
                    Meal.meal_type = "Lunch";
                    dbModel.tbl_meal.Add(Meal);
                    tbl_audit_trail audit = new tbl_audit_trail
                    {
                        table_name = "tbl_meal",
                        operation = "ADD",
                        occured_at = DateTime.Now,
                        performed_by = Session["FirstName"].ToString() + " " + Session["LastName"],
                        description = "Add " + Meal.meal_type
                    };
                    dbModel.tbl_audit_trail.Add(audit);
                    dbModel.SaveChanges();
                    var user2 = dbModel.tbl_meal.Where(x => x.id_daily == Meal.id_daily && x.meal_type == Meal.meal_type).FirstOrDefault();
                    Session["IdMeal"] = user2.id_meal;
                    return View(user2);
                }

            }
            else
            {
                message = "Invalid Request";
                ViewBag.Message = message;
                return View();
            }

        }

        public ActionResult Dinner(tbl_meal Meal)
        {
            string message = "";
            string dailyID = Session["IdDaily"].ToString();
            if (Session["IdDaily"] != null)
            {
                //int dailyID = Convert.ToInt32(Session["IdDaily"]);                
                var search = dbModel.tbl_daily.Where(a => a.id_hashing == dailyID).FirstOrDefault();
                var user = dbModel.tbl_meal.Where(x => x.id_daily == search.id_daily && x.meal_type == "Dinner").FirstOrDefault();
                if (user != null)
                {
                    Session["IdMeal"] = user.id_meal;
                    return View(user);
                }
                else
                {
                    //Meal.id_daily = Convert.ToInt32(Session["IdDaily"]);
                    var search2 = dbModel.tbl_daily.Where(a => a.id_hashing == dailyID).FirstOrDefault();
                    Meal.id_daily = search2.id_daily;
                    Meal.meal_type = "Dinner";
                    dbModel.tbl_meal.Add(Meal);
                    tbl_audit_trail audit = new tbl_audit_trail
                    {
                        table_name = "tbl_meal",
                        operation = "ADD",
                        occured_at = DateTime.Now,
                        performed_by = Session["FirstName"].ToString() + " " + Session["LastName"],
                        description = "Add " + Meal.meal_type
                    };
                    dbModel.tbl_audit_trail.Add(audit);
                    dbModel.SaveChanges();
                    var user2 = dbModel.tbl_meal.Where(x => x.id_daily == Meal.id_daily && x.meal_type == Meal.meal_type).FirstOrDefault();
                    Session["IdMeal"] = user2.id_meal;
                    return View(user2);
                }

            }
            else
            {
                message = "Invalid Request";
                ViewBag.Message = message;
                return View();
            }
        }

        [HttpGet]
        public JsonResult GetDataSelectFood()
        {
            using (db_enutritionEntities dbModel = new db_enutritionEntities())
            {
                //List<tbl_disease> diseaseList = dbModel.tbl_disease.ToList<tbl_disease>();
                //return Json(new { data = diseaseList }, JsonRequestBehavior.AllowGet);
                
                int IDTemp = Convert.ToInt32(Session["IdMeal"]);
               

                List<tbl_detail_food> DetailFoodList = dbModel.tbl_detail_food.Where(s => s.id_meal == IDTemp).ToList<tbl_detail_food>();
                List<tbl_food> FoodList = dbModel.tbl_food.ToList<tbl_food>();
                List<FoodSelectOutput> result = new List<FoodSelectOutput>();
                int tot = 0;
                foreach (var item in DetailFoodList)
                {
                    foreach (var item2 in FoodList)
                    {
                        
                        if (item.id_food == item2.id_food)
                        {
                            //tot = tot + Convert.ToInt32(item2.energy);
                            result.Add(new FoodSelectOutput
                            {
                                id_food = item2.id_food,
                                id_detail_food =  item.id_detail_food,
                                name_food = item2.name_food,
                                energy = Convert.ToInt32(item2.energy)
                                //total_energy = tot
                            });
                        }
                    }
                    
                }
                //TempData["IdMealPassing"] = IDTemp;
                //TempData["IdMealPassing3"] = IDTemp;
                return Json(new { data = result }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetData()
        {
            using (db_enutritionEntities dbModel = new db_enutritionEntities())
            {
                List<tbl_food> FoodList = dbModel.tbl_food.ToList<tbl_food>();
                List<FoodOutput> result = new List<FoodOutput>();
                foreach (var item in FoodList)
                {

                    result.Add(new FoodOutput
                    {
                        id_food = item.id_food,
                        name_food = item.name_food,
                        energy = Convert.ToInt32(item.energy),
                        protein = Convert.ToInt32(item.protein),
                        carbohydrate = Convert.ToInt32(item.carbohydrate),
                        fat = Convert.ToInt32(item.fat)
                    });
                }

                return Json(new { data = result }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult SelectFood(int id)
        {
            //int mealID = Convert.ToInt32(TempData["IdMeal"]);
            //TempData["IdMeal"] = mealID;
            return View(dbModel.tbl_food.Where(x => x.id_food == id).FirstOrDefault<tbl_food>());
        }

        [HttpPost]
        public ActionResult SelectFood(tbl_food Food)
        {
            tbl_detail_food DetailFood = new tbl_detail_food();
            
            //DetailFood.id_meal = Convert.ToInt32(TempData["IdMeal"]);

            #region MyRegion
            /*
                if (Convert.ToInt32(TempData["IdMealPassing"]) != 0)
                {
                    IDTemp = Convert.ToInt32(TempData["IdMealPassing"]);
                    //DetailFood.id_meal = Convert.ToInt32(TempData["IdMealPassing"]);
                }
                else if (Convert.ToInt32(TempData["IdMealPassing3"]) != 0)
                {
                    IDTemp = Convert.ToInt32(TempData["IdMealPassing3"]);
                    //DetailFood.id_meal = Convert.ToInt32(TempData["IdMealPassing3"]);
                }
                else if (Convert.ToInt32(TempData["IdMealPassing4"]) != 0)
                {
                    IDTemp = Convert.ToInt32(TempData["IdMealPassing4"]);
                    //DetailFood.id_meal = Convert.ToInt32(TempData["IdMealPassing3"]);
                }
                */
            #endregion
            int IDTemp = Convert.ToInt32(Session["IdMeal"]);
            string dailyID = Session["IdDaily"].ToString();
            var searchDaily = dbModel.tbl_daily.Where(c => c.id_hashing == dailyID).FirstOrDefault();
            //update data meal total energy
            var search = dbModel.tbl_meal.Where(a => a.id_meal == IDTemp).FirstOrDefault();
            if (search != null)
            {
                if (searchDaily != null)
                {
                    DetailFood.id_food = Food.id_food;
                    DetailFood.id_meal = IDTemp;
                    dbModel.tbl_detail_food.Add(DetailFood);
                    tbl_audit_trail audit = new tbl_audit_trail
                    {
                        table_name = "tbl_detail_food",
                        operation = "ADD",
                        occured_at = DateTime.Now,
                        performed_by = Session["FirstName"].ToString() + " " + Session["LastName"],
                        description = "Add Food to List Food"
                    };
                    dbModel.tbl_audit_trail.Add(audit);
                    dbModel.SaveChanges();
                    List<tbl_detail_food> DetailFoodList = dbModel.tbl_detail_food.Where(s => s.id_meal == IDTemp).ToList<tbl_detail_food>();
                    List<tbl_food> FoodList = dbModel.tbl_food.ToList<tbl_food>();
                    int tot = 0;
                    foreach (var item in DetailFoodList)
                    {
                        foreach (var item2 in FoodList)
                        {
                            if (item.id_food == item2.id_food)
                            {
                                tot = tot + Convert.ToInt32(item2.energy);
                            }
                        }
                    }


                    search.total_energy_meal = tot;
                    dbModel.Entry(search).State = EntityState.Modified;
                    dbModel.SaveChanges();
                    
                    List<tbl_meal> MealList = dbModel.tbl_meal.Where(m => m.id_daily == searchDaily.id_daily).ToList<tbl_meal>();
                    int totdaily = 0;
                    foreach (var item3 in MealList)
                    {
                        totdaily = totdaily + Convert.ToInt32(item3.total_energy_meal);
                    }                   
                    
                    int IDPatient = Convert.ToInt32(Session["IdPatient"]);
                    int min_energy = 1500;
                    string desc = "";
                    var user = dbModel.tbl_patient.Where(u => u.id_pantient == IDPatient).FirstOrDefault();
                    if (user != null)
                    {
                        int margin = 0;
                        if (min_energy>totdaily)
                        {
                            margin = min_energy - totdaily;
                            desc = "Less " + margin + " energy";
                        }
                        else if (totdaily>user.total_max_energy)
                        {
                            margin = totdaily - Convert.ToInt32(user.total_max_energy);
                            desc = "Over " + margin + " energy";
                        }
                        else
                        {
                            desc = "Normal";
                        }
                    }

                    searchDaily.description = desc;
                    searchDaily.total_energy_daily = totdaily;
                    dbModel.Entry(searchDaily).State = EntityState.Modified;
                    dbModel.SaveChanges();
                    return Json(new { success = true, message = "Food Select Successfully" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = "Food Select Failed" }, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json(new { success = false, message = "Food Select Failed" }, JsonRequestBehavior.AllowGet);
            }
            //TempData["IdMealPassing2"] = DetailFood.id_meal;
            //TempData["IdMealPassing3"] = DetailFood.id_meal;
            //TempData["IdMealPassing4"] = DetailFood.id_meal;
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {

            tbl_detail_food food = dbModel.tbl_detail_food.Where(x => x.id_detail_food == id).FirstOrDefault<tbl_detail_food>();
            
            int IDTemp = Convert.ToInt32(Session["IdMeal"]);
            string dailyID = Session["IdDaily"].ToString();
            var searchDaily = dbModel.tbl_daily.Where(c => c.id_hashing == dailyID).FirstOrDefault();
            //update data meal total energy
            var search = dbModel.tbl_meal.Where(a => a.id_meal == IDTemp).FirstOrDefault();
            if (search != null)
            {
                if (searchDaily != null)
                {
                    dbModel.tbl_detail_food.Remove(food);
                    tbl_audit_trail audit = new tbl_audit_trail
                    {
                        table_name = "tbl_detail_food",
                        operation = "DELETE",
                        occured_at = DateTime.Now,
                        performed_by = Session["FirstName"].ToString() + " " + Session["LastName"],
                        description = "Delete Food to List Food"
                    };
                    dbModel.tbl_audit_trail.Add(audit);
                    dbModel.SaveChanges();
                    List<tbl_detail_food> DetailFoodList = dbModel.tbl_detail_food.Where(s => s.id_meal == IDTemp).ToList<tbl_detail_food>();
                    List<tbl_food> FoodList = dbModel.tbl_food.ToList<tbl_food>();
                    int tot = 0;
                    foreach (var item in DetailFoodList)
                    {
                        foreach (var item2 in FoodList)
                        {
                            if (item.id_food == item2.id_food)
                            {
                                tot = tot + Convert.ToInt32(item2.energy);
                            }
                        }
                    }
                    search.total_energy_meal = tot;
                    dbModel.Entry(search).State = EntityState.Modified;
                    dbModel.SaveChanges();

                    List<tbl_meal> MealList = dbModel.tbl_meal.Where(m => m.id_daily == searchDaily.id_daily).ToList<tbl_meal>();
                    int totdaily = 0;
                    foreach (var item3 in MealList)
                    {
                        totdaily = totdaily + Convert.ToInt32(item3.total_energy_meal);
                    }

                    int IDPatient = Convert.ToInt32(Session["IdPatient"]);
                    int min_energy = 1500;
                    string desc = "";
                    var user = dbModel.tbl_patient.Where(u => u.id_pantient == IDPatient).FirstOrDefault();
                    if (user != null)
                    {
                        int margin = 0;
                        if (min_energy > totdaily)
                        {
                            margin = min_energy - totdaily;
                            desc = "Less " + margin + " energy";
                        }
                        else if (totdaily > user.total_max_energy)
                        {
                            margin = totdaily - Convert.ToInt32(user.total_max_energy);
                            desc = "Over " + margin + " energy";
                        }
                        else
                        {
                            desc = "Normal";
                        }
                    }

                    searchDaily.description = desc;
                    searchDaily.total_energy_daily = totdaily;
                    dbModel.Entry(searchDaily).State = EntityState.Modified;
                    dbModel.SaveChanges();
                    return Json(new { success = true, message = "Delete Successfully" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = "Delete Failed" }, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json(new { success = false, message = "Delete Failed" }, JsonRequestBehavior.AllowGet);
            }                   
                       
        }

        public class FoodSelectOutput
        {
            public int id_food { get; set; }
            public int id_detail_food { get; set; }
            public string name_food { get; set; }
            public int energy { get; set; }
            //public int total_energy { get; set; }
        }

        public class FoodOutput
        {
            public int id_food { get; set; }
            public string name_food { get; set; }
            public int energy { get; set; }
            public int carbohydrate { get; set; }
            public int protein { get; set; }
            public int fat { get; set; }
        }
    }
}