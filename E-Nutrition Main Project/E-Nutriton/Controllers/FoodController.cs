﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using E_Nutriton.Models;
using System.Data.Entity;


namespace E_Nutriton.Controllers
{
    public class FoodController : Controller
    {
        db_enutritionEntities dbModel = new db_enutritionEntities();

        // GET: food
        public ActionResult Index()
        {
            ViewBag.Message = TempData["Message"];
            return View();
        }
        
        [HttpGet]
        public JsonResult GetData()
        {
            using (db_enutritionEntities dbModel = new db_enutritionEntities())
            {
                List<tbl_food> FoodList = dbModel.tbl_food.ToList<tbl_food>();
                List<FoodOutput> result = new List<FoodOutput>();
                foreach (var item in FoodList)
                {
                    
                    result.Add(new FoodOutput {
                        id_food = item.id_food,
                        name_food = item.name_food,
                        portion = Convert.ToInt32(item.portion),
                        energy = Convert.ToInt32(item.energy),
                        protein = Convert.ToInt32(item.protein),
                        carbohydrate = Convert.ToInt32(item.carbohydrate),
                        fat = Convert.ToInt32(item.fat)
                    });                     
                }

                return Json(new { data = result.OrderByDescending(e => e.id_food) }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult AddorEdit(int id = 0)
        {
            if (id == 0)
                return View(new tbl_food());
            else
            {
                using (db_enutritionEntities dbModel = new db_enutritionEntities())
                {
                    return View(dbModel.tbl_food.Where(x => x.id_food == id).FirstOrDefault<tbl_food>());
                }
            }
        }

        [HttpPost]
        public ActionResult AddorEdit(tbl_food food)
        {
            using (var transaction = dbModel.Database.BeginTransaction())
            {
                try
                {
                    if (food.id_food == 0)
                    {
                        var foodsearch = dbModel.tbl_food.Where(a => a.name_food == food.name_food && a.portion == food.portion).FirstOrDefault();
                        if (foodsearch == null)
                        {
                            dbModel.tbl_food.Add(food);
                            tbl_audit_trail audit = new tbl_audit_trail
                            {
                                table_name = "tbl_food",
                                operation = "ADD",
                                occured_at = DateTime.Now,
                                performed_by = Session["Username"].ToString(),
                                description = "Add new food " + food.name_food
                            };
                            dbModel.tbl_audit_trail.Add(audit);
                            dbModel.SaveChanges();
                            transaction.Commit();
                            return Json(new { success = true, message = "Saved Successfully" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            TempData["Message"] = "The food data already exists";
                            transaction.Commit();
                            return Json(new { success = false, message = "The food data already exists" }, JsonRequestBehavior.AllowGet);
                        }
                        
                    }
                    else
                    {
                        var foodsearch = dbModel.tbl_food.Where(a => a.name_food == food.name_food && a.portion == food.portion && a.id_food != food.id_food).FirstOrDefault();
                        if (foodsearch == null)
                        {
                            dbModel.Entry(food).State = EntityState.Modified;
                            tbl_audit_trail audit = new tbl_audit_trail
                            {
                                table_name = "tbl_food",
                                operation = "EDIT",
                                occured_at = DateTime.Now,
                                performed_by = Session["Username"].ToString(),
                                description = "Edit Food " + food.name_food
                            };
                            dbModel.tbl_audit_trail.Add(audit);
                            dbModel.SaveChanges();
                            transaction.Commit();
                            return Json(new { success = true, message = "Update Successfully" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            TempData["Message"] = "The food data already exists";
                            transaction.Commit();
                            return Json(new { success = false, message = "The food data already exists" }, JsonRequestBehavior.AllowGet);
                        }

                    }
                }
                catch (Exception msgError)
                {
                    db_enutritionEntities dbModel2 = new db_enutritionEntities();

                    tbl_error_log errlog = new tbl_error_log()
                    {
                        error_line = msgError.StackTrace,
                        error_time = DateTime.Now,
                        performed_by = Session["Username"].ToString(),
                        error_description = msgError.Message
                    };
                    dbModel2.tbl_error_log.Add(errlog);
                    dbModel2.SaveChanges();
                    transaction.Rollback();
                    return Json(new { success = true, message = "Error " + msgError.Message }, JsonRequestBehavior.AllowGet);
                } 
            }

        }

        public ActionResult Delete(int id)
        {
            using (db_enutritionEntities dbModel = new db_enutritionEntities())
            {
                tbl_food food = dbModel.tbl_food.Where(x => x.id_food == id).FirstOrDefault<tbl_food>();
                dbModel.tbl_food.Remove(food);
                tbl_audit_trail audit = new tbl_audit_trail
                {
                    table_name = "tbl_food",
                    operation = "DELETE",
                    occured_at = DateTime.Now,
                    performed_by = Session["Username"].ToString(),
                    description = "Delete Food " + food.name_food
                };
                dbModel.tbl_audit_trail.Add(audit);

                dbModel.SaveChanges();
                return Json(new { success = true, message = "delete Successfully" }, JsonRequestBehavior.AllowGet);

            }

        }

        public class FoodOutput
        {            
            public int id_food { get; set; }
            public string name_food { get; set; }
            public int portion { get; set; }
            public int energy { get; set; }
            public int carbohydrate { get; set; }
            public int protein { get; set; }
            public int fat { get; set; }
        }

    }
}