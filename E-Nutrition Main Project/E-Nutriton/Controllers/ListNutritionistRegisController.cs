﻿using E_Nutriton.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace E_Nutriton.Controllers
{
    public class ListNutritionistRegisController : Controller
    {
        // GET: ListNutritionistRegis
        db_enutritionEntities dbModel = new db_enutritionEntities();
        [HttpGet]
        [Authorize]
        public ActionResult Index()
        {
            var userNutritionist = dbModel.tbl_nutritionist.Where(a => a.is_account_active == false && a.certificate != "~/img/certificate/default-certificate.jpg").ToList();
            return View(userNutritionist);
        }

        [HttpPost]
        public ActionResult Select(int id)
        {
            try
            {
                // TODO: Add update logic here

                string message = "";
                var user = dbModel.tbl_nutritionist.Where(x => x.id_nutritionist == id).FirstOrDefault();
                if (user != null)
                {
                    dbModel.Configuration.ValidateOnSaveEnabled = false; // this line to avoid confirm password does not
                    user.is_account_active = true;
                    dbModel.SaveChanges();
                }
                else
                {
                    message = "User Not Found";
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        [HttpGet]
        public ActionResult Select(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            db_enutritionEntities dbModel = new db_enutritionEntities();
            if (dbModel == null)
            {
                return HttpNotFound();
            }
            return View();
        }
    }
}