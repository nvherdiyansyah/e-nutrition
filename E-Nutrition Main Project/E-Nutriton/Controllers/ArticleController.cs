﻿using E_Nutriton.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;


namespace E_Nutriton.Controllers
{
    [RoutePrefix("Article")]
    [ValidateInput(false)]
    public class ArticleController : Controller
    {
     
            private db_enutritionEntities dbModel = new db_enutritionEntities();
            // GET: Article
            [Authorize]
            public ActionResult Index()
            {
                var content = dbModel.tbl_article.Select(s => new
                {
                    s.Id_article,
                    s.Title,
                    s.Picture,
                    s.Content,
                    s.Release_date
                
                });

                List<ArticleViewModel> contentModel = content.Select(item => new ArticleViewModel()
                {
                    Id_article = item.Id_article,
                    Title = item.Title,
                    Picture = item.Picture,
                    Content = item.Content,
                    Release_date = item.Release_date
                    
                }).ToList();
                return View(contentModel);
            }




        // GET: Article/Details/5
        [Authorize]
        public ActionResult Details(int id)
        {
            return View(dbModel.tbl_article.Where(x => x.Id_article == id).FirstOrDefault());
        }

        // GET: Article/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Article/Create
        [Route("Create")]
        [HttpPost]
        [Authorize]
        public ActionResult Create(ArticleViewModel model)
        {
            try
            {
                HttpPostedFileBase file = Request.Files["ImageData"];
                ArticleRepositories service = new ArticleRepositories();
                int i = service.UploadImageInDataBase(file, model);
                if (i == 1)
                {
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch
            {
                return View();
            }
        }

        // GET: Article/Edit/5
        public ActionResult Edit(int Id)
        {
            var user = dbModel.tbl_article.Where(s => s.Id_article == Id).FirstOrDefault();
            return View(user);

        }

        [HttpPost]
        public ActionResult Edit(int id, ArticleViewModel model)
        {
            try
            {

                HttpPostedFileBase file = Request.Files["ImageData"];
                ArticleRepositories service = new ArticleRepositories();
                int i = service.UploadImageInDataBase(file, model);
                if (i == 1)
                {
                    return RedirectToAction("Index");
                }
                return View(model);
               
            }
            catch
            {
                return View();
            }
        }


        // GET: Article/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }


        // POST: Article/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, tbl_article article)
        {
            try
            {
                // TODO: Add delete logic here
                article = dbModel.tbl_article.Where(b => b.Id_article == id).FirstOrDefault();
                dbModel.tbl_article.Remove(article);
                dbModel.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {

                return View();
            }
        }

        // GET: Customer/Details/5
        public ActionResult Detail(int id)
        {
            return View(dbModel.tbl_article.Where(b => b.Id_article == id).FirstOrDefault());
        }


        public ActionResult RetrieveImage(int id)
        {
            byte[] cover = GetImageFromDataBase(id);
            if (cover != null)
            {
                return File(cover, "image/jpg");
            }
            else
            {
                return null;
            }
        }

        public byte[] GetImageFromDataBase(int Id)
        {
            var q = from temp in dbModel.tbl_article where temp.Id_article == Id select temp.Picture;
            byte[] cover = q.First();
            return cover;
        }


    }
}
