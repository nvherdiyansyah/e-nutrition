﻿using E_Nutriton.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Nutriton.Controllers
{
    public class ListPatientController : Controller
    {
        // GET: ListPatient
        db_enutritionEntities dbModel = new db_enutritionEntities();
        [HttpGet]
        [Authorize]
        public ActionResult Index()
        {
            Session["IdHasingPatient"] = null;
            int IDNutritionist = Convert.ToInt32(Session["IdNutritionist"]);
            var user = dbModel.tbl_patient.Where(a => a.id_nutritionist == IDNutritionist && a.is_account_active == true).ToList();
            return View(user);
        }


        [HttpGet]
        [Authorize]
        public ActionResult Select(string id)
        {
            Session["IdHasingPatient"] = id;
            Session["IdHasingDaily"] = null;
            Session["IdDaly"] = null;
            var searchuser = dbModel.tbl_patient.Where(s => s.id_hashing == id).FirstOrDefault();
            int IDNutritionist = Convert.ToInt32(Session["IdNutritionist"]);
            var user = dbModel.tbl_daily.Where(a => a.id_pantient == searchuser.id_pantient).ToList();
            return View(user);
        }

        [HttpGet]
        [Authorize]
        public ActionResult Detail(string id)
        {
            Session["IdHasingDaily"] = id;
            var searchuser = dbModel.tbl_daily.Where(s => s.id_hashing == id).FirstOrDefault();
            int IDNutritionist = Convert.ToInt32(Session["IdNutritionist"]);
            Session["IdDaly"] = searchuser.id_daily;
            var user = dbModel.tbl_meal.Where(a => a.id_daily == searchuser.id_daily).ToList();

            ViewBag.Message = TempData["Message"];

            return View(user);
        }

        [HttpGet]
        [Authorize]
        public ActionResult ListFood(int? id)
        {
            int IDDaily = Convert.ToInt32(Session["IdDaly"]);
            var detailfood = dbModel.tbl_detail_food.Where(a => a.id_meal == id).FirstOrDefault();

            
            if (detailfood != null)
            {
                var ListFood = dbModel.vw_list_detail_food.Where(b => b.id_daily == IDDaily && b.id_meal == detailfood.id_meal).ToList();
                return View(ListFood);
            }
            else
            {
                TempData["Message"] = "Food list is empety";
                return RedirectToAction("Detail");
            }
            

        }

        [HttpGet]
        [Authorize]
        public ActionResult Advice(string id)
        {
            using (var transaction = dbModel.Database.BeginTransaction())
            {
                try
                {
                    //Session["IdHasingDaily"] = id;
                    var searchuser = dbModel.tbl_daily.Where(s => s.id_hashing == id).FirstOrDefault();
                    //int IDDaily = Convert.ToInt32(Session["IdDaly"]);
                    int IDNutritionist = Convert.ToInt32(Session["IdNutritionist"]);
                    var searchAdvice = dbModel.tbl_advice.Where(a => a.id_daily == searchuser.id_daily && a.id_nutritionist == IDNutritionist).FirstOrDefault();
                    if (searchAdvice != null)
                    {
                        return View(searchAdvice);
                    }
                    else
                    {
                        tbl_advice Advice = new tbl_advice();
                        Advice.id_daily = searchuser.id_daily;
                        Advice.id_nutritionist = IDNutritionist;
                        dbModel.tbl_advice.Add(Advice);
                        tbl_audit_trail audit = new tbl_audit_trail
                        {
                            table_name = "tbl_advice",
                            operation = "ADD",
                            occured_at = DateTime.Now,
                            performed_by = Session["FirstName"].ToString() + " " + Session["LastName"],
                            description = "Add advice"
                        };
                        dbModel.tbl_audit_trail.Add(audit);
                        dbModel.SaveChanges();
                        var searchAdvice2 = dbModel.tbl_advice.Where(b => b.id_daily == Advice.id_daily && b.id_nutritionist == Advice.id_nutritionist).FirstOrDefault();
                        transaction.Commit();
                        return View(searchAdvice2);
                    }
                }
                catch (Exception msgError)
                {
                    db_enutritionEntities dbModel2 = new db_enutritionEntities();

                    tbl_error_log errlog = new tbl_error_log()
                    {
                        error_line = msgError.StackTrace,
                        error_time = DateTime.Now,
                        performed_by = Session["FirstName"].ToString() + " " + Session["LastName"],
                        error_description = msgError.Message
                    };
                    dbModel2.tbl_error_log.Add(errlog);
                    dbModel2.SaveChanges();

                    ViewBag.Message = msgError.Message;
                    ViewBag.Status = false;

                    transaction.Rollback();
                    return View();
                } 
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult Advice(tbl_advice adviceModel)
        {
            using (var transaction = dbModel.Database.BeginTransaction())
            {
                try
                {
                    // TODO: Add update logic here
                    var searchAdvice = dbModel.tbl_advice.Where(c => c.id_advice == adviceModel.id_advice).FirstOrDefault();
                    searchAdvice.advice = adviceModel.advice;
                    dbModel.Entry(searchAdvice).State = EntityState.Modified;
                    tbl_audit_trail audit = new tbl_audit_trail
                    {
                        table_name = "tbl_advice",
                        operation = "EDIT",
                        occured_at = DateTime.Now,
                        performed_by = Session["FirstName"].ToString() + " " + Session["LastName"],
                        description = "Edit advice"
                    };
                    dbModel.tbl_audit_trail.Add(audit);
                    dbModel.SaveChanges();                    
                    transaction.Commit();
                    return View(searchAdvice);
                }
                catch (Exception msgError)
                {
                    db_enutritionEntities dbModel2 = new db_enutritionEntities();

                    tbl_error_log errlog = new tbl_error_log()
                    {
                        error_line = msgError.StackTrace,
                        error_time = DateTime.Now,
                        performed_by = Session["FirstName"].ToString() + " " + Session["LastName"],
                        error_description = msgError.Message
                    };
                    dbModel2.tbl_error_log.Add(errlog);
                    dbModel2.SaveChanges();

                    ViewBag.Message = msgError.Message;
                    ViewBag.Status = false;

                    transaction.Rollback();
                    return View();
                } 
            }
        }
    }
}