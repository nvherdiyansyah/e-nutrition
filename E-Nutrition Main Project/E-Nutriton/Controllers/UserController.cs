﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using E_Nutriton.Models;

namespace E_Nutriton.Controllers
{
    public class UserController : Controller
    {
        // Registration Action
        db_enutritionEntities dbModel = new db_enutritionEntities();
        [HttpGet]
        public ActionResult Registration()
        {
            return View();
        }

        // Registration POST action
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registration([Bind(Exclude = "is_email_valid,activation_code,is_account_active")] tbl_nutritionist NutritionistModel)
        {
            using (var transaction = dbModel.Database.BeginTransaction())
            {
                try
                {
                    bool Status = false;
                    string Message = "";
                    // Model Validation
                    if (ModelState.IsValid)
                    {
                        #region // Email is already exist 
                        var isExist = IsEmailExist(NutritionistModel.email);
                        if (isExist)
                        {
                            ModelState.AddModelError("EmailExist", "Email already exist");
                            return View();
                        }
                        #endregion

                        #region // Generate Activation Code
                        NutritionistModel.activation_code = Guid.NewGuid();
                        #endregion

                        #region // Password Hassing
                        NutritionistModel.password = Crypto.Hash(NutritionistModel.password);
                        NutritionistModel.confirm_password = Crypto.Hash(NutritionistModel.confirm_password);
                        #endregion

                        NutritionistModel.is_email_valid = false;
                        NutritionistModel.picture = "~/img/picture/default_picture.jpg";
                        NutritionistModel.certificate = "~/img/certificate/default-certificate.jpg";
                        NutritionistModel.address = "";
                        string id_hash = Guid.NewGuid().ToString().Remove(8);
                        NutritionistModel.id_hashing = id_hash;
                        NutritionistModel.is_account_active = false;

                        #region // Save to database
                        dbModel.tbl_nutritionist.Add(NutritionistModel);
                        dbModel.SaveChanges();
                        #endregion


                        // Send Email to User Nutristionist
                        SendVerificationLinkEmail(NutritionistModel.email, NutritionistModel.activation_code.ToString());
                        Message = "Registration successfully done. Account activation link " +
                            "has been seen to your email : " + NutritionistModel.email;
                        Status = true;
                    }
                    else
                    {
                        Message = "Invalid Request";
                        ViewBag.Message = Message;
                        ViewBag.Status = Status;
                        transaction.Commit();
                        return View();
                    }

                    ViewBag.Message = Message;
                    ViewBag.Status = Status;
                    TempData["Message"] = Message;
                    TempData["Status"] = Status;
                    transaction.Commit();
                    return RedirectToAction("Verifemail");
                }
                catch (Exception msgError)
                {
                    db_enutritionEntities dbModel2 = new db_enutritionEntities();

                    tbl_error_log errlog = new tbl_error_log()
                    {
                        error_line = msgError.StackTrace,
                        error_time = DateTime.Now,
                        error_description = msgError.Message
                    };
                    dbModel2.tbl_error_log.Add(errlog);
                    dbModel2.SaveChanges();
                    
                    ViewBag.Message = msgError.Message;
                    ViewBag.Status = false;
                    transaction.Rollback();
                    return View();
                }
            }
        }

        //untuk alihkan halaman regis

        public ActionResult Verifemail()
        {
            ViewBag.Message = TempData["Message"];
            ViewBag.Status = TempData["Status"];
            return View();
        }




        // Verify Account 
        [HttpGet]
        public ActionResult VerifyAccount(string id)
        {
            using (var transaction = dbModel.Database.BeginTransaction())
            {
                try
                {
                    bool Status = false;
                    dbModel.Configuration.ValidateOnSaveEnabled = false; // this line to avoid confirm password does not
                                                                         // match issue on save change
                    var v = dbModel.tbl_nutritionist.Where(a => a.activation_code == new Guid(id)).FirstOrDefault();
                    if (v != null)
                    {
                        v.is_email_valid = true;
                        dbModel.SaveChanges();
                        Status = true;
                    }
                    else
                    {
                        ViewBag.Message = "Invalid Request";
                    }

                    ViewBag.Status = Status;
                    transaction.Commit();
                    return View();
                }
                catch (Exception msgError)
                {
                    transaction.Rollback();
                    ViewBag.Message = msgError.Message;
                    ViewBag.Status = false;
                    return View();
                }
            }
        }

        // Login
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        // Login POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(UserLogin login, string ReturnUrl)
        {
            using (var transaction = dbModel.Database.BeginTransaction())
            {
                try
                {
                    string message = "";

                    var v = dbModel.tbl_nutritionist.Where(a => a.email == login.email).FirstOrDefault();
                    if (v != null)
                    {
                        if (string.Compare(Crypto.Hash(login.password), v.password) == 0)
                        {
                            if (v.is_email_valid == true)
                            {
                                // make time out for cookie login with Remember Me
                                int timeout = login.rememberme ? 525600 : 20; // 525600 minutes is 1 year
                                var ticket = new FormsAuthenticationTicket(login.email, login.rememberme, timeout);
                                string encrypted = FormsAuthentication.Encrypt(ticket);
                                var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted);
                                cookie.Expires = DateTime.Now.AddMinutes(timeout);
                                cookie.HttpOnly = true;
                                Response.Cookies.Add(cookie);

                                if (Url.IsLocalUrl(ReturnUrl))
                                {
                                    return Redirect(ReturnUrl);
                                }
                                else
                                {
                                    ViewBag.IsAccoutActive = v.is_account_active;
                                    SessionUpdate(v);
                                    return RedirectToAction("Index", "DashboardNutritionist");
                                }
                            }
                            else
                            {
                                message = "Your email is not active, please activate your email";
                            }
                        }
                        else
                        {
                            message = "Your password is incorrect";
                        }
                    }
                    else
                    {
                        message = "Your email is wrong or not registered ";
                    }

                    ViewBag.Message = message;
                    transaction.Commit();
                    return View();
                }
                catch (Exception msgError)
                {
                    db_enutritionEntities dbModel2 = new db_enutritionEntities();

                    tbl_error_log errlog = new tbl_error_log()
                    {
                        error_line = msgError.StackTrace,
                        error_time = DateTime.Now,
                        error_description = msgError.Message
                    };
                    dbModel2.tbl_error_log.Add(errlog);
                    dbModel2.SaveChanges();

                    ViewBag.Message = msgError.Message;
                    transaction.Rollback();
                    return View();
                }
            }
        }

        // Logout
        [Authorize]
        [HttpPost]
        public ActionResult Logout()
        {
            try
            {
                FormsAuthentication.SignOut();
                Session.Clear();
                Session.Abandon();
                Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Cache.SetNoStore();
                return RedirectToAction("Login", "User");
            }
            catch (Exception msgError)
            {
                ViewBag.Message = msgError.Message;
                return View();
            }
        }

        [NonAction]
        public bool IsEmailExist(string Email)
        {
            var v = dbModel.tbl_nutritionist.Where(a => a.email == Email).FirstOrDefault();
            return v != null;
        }

        [NonAction]
        public void SendVerificationLinkEmail(string Email, string ActivationCode, string emailfor = "VerifyAccount")
        {
            try
            {
                var verifyUrl = "/User/" + emailfor + "/" + ActivationCode;
                var checkUrl = Request.Url.AbsoluteUri;
                if (checkUrl.Contains("/E-Nutrition/"))
                {
                    verifyUrl = "/E-Nutrition/User/" + emailfor + "/" + ActivationCode;
                }

                var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);

                var fromEmail = new MailAddress("project.enutrition@gmail.com", "E-Nutrition");
                var toEmail = new MailAddress(Email);
                var fromEmailPassword = "nawaB7sln"; // Replace with actual password

                string subject = "";
                string body = "";
                if (emailfor == "VerifyAccount")
                {
                    subject = "Your E-Nutrition account is successfully created!";
                    body = "<br/><br/>We are excited to tell you that your E-Nutrition account is " +
                        "successfully created. Please click on the below link to verify your account" +
                        "<br/><br/><a href='" + link + "'>" + link + "</a>";
                }
                else if (emailfor == "ResetPassword")
                {
                    subject = "Reset Password";
                    body = "<br/><br/>We got request for reset your account password. Please click on below link to reset your password " +
                        "<br/><br/><a href='" + link + "'>Reset Password</a>";
                }

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
                };

                using (var message = new MailMessage(fromEmail, toEmail)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                })

                    smtp.Send(message);
            }
            catch (Exception msgError)
            {
                ViewBag.Message = msgError.Message;
            }
        }


        // Forgot Password
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ForgotPassword(string email)
        {
            using (var transaction = dbModel.Database.BeginTransaction())
            {
                try
                {
                    // Verify email
                    // Generet Reset Password Link
                    // Send Email
                    string message = "";
                    bool status = false;

                    var account = dbModel.tbl_nutritionist.Where(a => a.email == email).FirstOrDefault();
                    if (account != null)
                    {
                        // Send emaail for reset password
                        string resetCode = Guid.NewGuid().ToString();
                        SendVerificationLinkEmail(account.email, resetCode, "ResetPassword");
                        account.reset_password_code = resetCode;
                        // This line added here for to avoid confirm password not match issue, because confirm password property in model
                        dbModel.Configuration.ValidateOnSaveEnabled = false;
                        dbModel.SaveChanges();
                        message = "Reset password link has been sent to your email : " + email;
                        Session["ResetPassword"] = message;
                        //ViewBag.Message = message;
                        transaction.Commit();
                        return RedirectToAction("Login", "User");
                    }
                    else
                    {
                        message = "Account not found";
                    }
                    ViewBag.Message = message;
                    transaction.Commit();
                    return View();
                }
                catch (Exception msgError)
                {
                    db_enutritionEntities dbModel2 = new db_enutritionEntities();

                    tbl_error_log errlog = new tbl_error_log()
                    {
                        error_line = msgError.StackTrace,
                        error_time = DateTime.Now,
                        error_description = msgError.Message
                    };
                    dbModel2.tbl_error_log.Add(errlog);
                    dbModel2.SaveChanges();
                    
                    ViewBag.Message = msgError.Message;
                    transaction.Rollback();
                    return View();
                }
            }
        } 

        public ActionResult ResetPassword(string id)
        {
            using (var transaction = dbModel.Database.BeginTransaction())
            {
                try
                {
                    // Verify the reset password link
                    // Find account associated with this link
                    // Redicect to reset password page
                    var user = dbModel.tbl_nutritionist.Where(a => a.reset_password_code == id).FirstOrDefault();

                    if (user != null)
                    {
                        ResetPasswordModel model = new ResetPasswordModel();
                        model.reset_code = id;
                        transaction.Commit();
                        return View(model);
                    }
                    else
                    {
                        transaction.Commit();
                        return HttpNotFound();
                    }
                }
                catch (Exception msgError)
                {
                    db_enutritionEntities dbModel2 = new db_enutritionEntities();

                    tbl_error_log errlog = new tbl_error_log()
                    {
                        error_line = msgError.StackTrace,
                        error_time = DateTime.Now,
                        error_description = msgError.Message
                    };
                    dbModel2.tbl_error_log.Add(errlog);
                    dbModel2.SaveChanges();
                    
                    ViewBag.Message = msgError.Message;
                    transaction.Rollback();
                    return View();
                }
            }                
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordModel model)
        {
            using (var transaction = dbModel.Database.BeginTransaction())
            {
                try
                {
                    var message = "";
                    if (ModelState.IsValid)
                    {
                        var user = dbModel.tbl_nutritionist.Where(a => a.reset_password_code == model.reset_code).FirstOrDefault();
                        if (user != null)
                        {
                            user.password = Crypto.Hash(model.new_password);
                            user.reset_password_code = "";
                            dbModel.Configuration.ValidateOnSaveEnabled = false;
                            dbModel.SaveChanges();
                            message = "New password updated successfully";
                        }
                    }
                    else
                    {
                        message = "Invalid Request";
                    }
                    ViewBag.Message = message;
                    transaction.Commit();
                    return View(model);
                }
                catch (Exception msgError)
                {
                    db_enutritionEntities dbModel2 = new db_enutritionEntities();

                    tbl_error_log errlog = new tbl_error_log()
                    {
                        error_line = msgError.StackTrace,
                        error_time = DateTime.Now,
                        error_description = msgError.Message
                    };
                    dbModel2.tbl_error_log.Add(errlog);
                    dbModel2.SaveChanges();
                    
                    ViewBag.Message = msgError.Message;
                    transaction.Rollback();
                    return View();
                }
            }
        }

        // Session Update
        [NonAction]
        private void SessionUpdate(tbl_nutritionist user)
        {
            Session["LoginNutritionist"] = "Login";
            Session["IdNutritionist"] = user.id_nutritionist.ToString();
            Session["FirstName"] = user.first_name.ToString();
            Session["LastName"] = user.last_name.ToString();
            Session["Picture"] = user.picture.ToString();
            Session["IsAccountActive"] = user.is_account_active;
        }
    }
}