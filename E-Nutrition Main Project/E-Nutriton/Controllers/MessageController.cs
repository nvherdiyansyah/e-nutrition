﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Nutriton.Controllers
{
    public class MessageController : Controller
    {
        // GET: Message
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Chat()
        {
            if (Session["LoginNutritionist"] != null)
            {
                Session["ChatName"] = "Nutritionist(" + Session["FirstName"].ToString() + ")";
                return View();
            }
            else if (Session["LoginPatient"] != null)
            {
                Session["ChatName"] = "Patient(" + Session["FirstName"].ToString() + ")";
                return View();
            }
            else
            {
                return View();
            }
            
        }
    }
}