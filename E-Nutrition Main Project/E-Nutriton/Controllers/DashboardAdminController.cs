﻿using E_Nutriton.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Nutriton.Controllers
{
    public class DashboardAdminController : Controller
    {
        // GET: DashboardAdmin
        db_enutritionEntities dbModel = new db_enutritionEntities();

        [Authorize]
        public ActionResult Index()
        {
            if(Session["LoginAdmin"] != null)
            {
                var userpatient = dbModel.tbl_patient.Where(a => a.is_account_active == true).ToList();
                var usernutritionist = dbModel.tbl_nutritionist.Where(a => a.is_account_active == true).ToList();
                ViewBag.TotPatient = userpatient.Count().ToString();
                ViewBag.TotNutritionist = usernutritionist.Count().ToString();
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Admin");
            }
        }

        public ActionResult ListPatient()
        {
            var userpatient = dbModel.tbl_patient.Where(a => a.is_account_active == true).ToList();
            return View(userpatient);
        }

        public ActionResult ListNutritionist()
        {
            var usernutritionist = dbModel.tbl_nutritionist.Where(a => a.is_account_active == true).ToList();
            return View(usernutritionist);
        }

    }
}