﻿using E_Nutriton.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace E_Nutriton.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        db_enutritionEntities dbModel = new db_enutritionEntities();
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(AdminLogin login, string ReturnUrl)
        {
            string message = "";

            var user = dbModel.tbl_admin.Where(a => a.email == login.username_email || a.username == login.username_email).FirstOrDefault();
            if (user != null)
            {
                if (user.is_email_valid == true || user.is_super_admin == true)
                {
                    int timeout = login.rememberme ? 525600 : 20; // 525600 minutes is 1 year
                    var ticket = new FormsAuthenticationTicket(login.username_email, login.rememberme, timeout);
                    string encrypted = FormsAuthentication.Encrypt(ticket);
                    var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted);
                    cookie.Expires = DateTime.Now.AddMinutes(timeout);
                    cookie.HttpOnly = true;
                    Response.Cookies.Add(cookie);

                    if (Url.IsLocalUrl(ReturnUrl))
                    {
                        return Redirect(ReturnUrl);
                    }
                    else
                    {
                        if(user.is_super_admin == true)
                        {
                            Session["IsSuperAdmin"] = user.is_super_admin;
                        }
                        ViewBag.IsSuperAdmin = user.is_super_admin;
                        SessionUpdate(user);
                        return RedirectToAction("Index", "DashboardAdmin");
                    }
                }
                else
                {
                    message = "Your email is not active, please activate your email";
                }
            }

            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Logout()
        {
            try
            {
                FormsAuthentication.SignOut();
                Session["LoginNutritionist"] = null;
                return RedirectToAction("Index", "Admin");
            }
            catch (Exception msgError)
            {
                ViewBag.Message = msgError.Message;
                return View();
            }
        }

        [NonAction]
        private void SessionUpdate(tbl_admin user)
        {
            Session["LoginAdmin"] = "Login";
            Session["IdAdmin"] = user.id_admin.ToString();
            Session["FullName"] = user.full_name.ToString();
            Session["Username"] = user.username.ToString();
            //Session["Email"] = user.email.ToString();
        }
    }
}