﻿using E_Nutriton.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Nutriton.Controllers
{
    public class SelectDiseaseController : Controller
    {
        db_enutritionEntities dbModel = new db_enutritionEntities();
        // GET: SelectDisease
        [Authorize]
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetData()
        {
            List<tbl_disease> diseaseList = dbModel.tbl_disease.ToList<tbl_disease>();
            List<DiseaseOutput> result = new List<DiseaseOutput>();

            foreach (var item in diseaseList)
            {

                result.Add(new DiseaseOutput
                {
                    id_disease = item.id_disease,
                    name_disease = item.name_disease,
                    energy = Convert.ToInt32(item.energy)
                });
            }
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetDataSelectDisease()
        {
            int IDTemp = Convert.ToInt32(Session["IdPatient"]);

            List<tbl_detail_disease> DetailDiseaseList = dbModel.tbl_detail_disease.Where(s => s.id_pantient == IDTemp).ToList<tbl_detail_disease>();
            List<tbl_disease> FoodList = dbModel.tbl_disease.ToList<tbl_disease>();
            List<SelectDiseaseOutput> result = new List<SelectDiseaseOutput>();
            
            foreach (var item in DetailDiseaseList)
            {
                foreach (var item2 in FoodList)
                {

                    if (item.id_disease == item2.id_disease)
                    {
                        result.Add(new SelectDiseaseOutput
                        {
                            id_disease = item2.id_disease,
                            id_detail_disease = item.id_detail_disease,
                            name_disease = item2.name_disease,
                            energy = Convert.ToInt32(item2.energy)                            
                        });
                    }
                }

            }
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Select(int id)
        {
            return View(dbModel.tbl_disease.Where(x => x.id_disease == id).FirstOrDefault<tbl_disease>());
        }

        [HttpPost]
        public ActionResult Select(tbl_disease Disease)
        {
            tbl_detail_disease DetailDisease = new tbl_detail_disease();
            
            int IDTemp = Convert.ToInt32(Session["IdPatient"]);
            //update data meal total energy
            var search = dbModel.tbl_patient.Where(a => a.id_pantient == IDTemp).FirstOrDefault();
            if (search != null)
            {
                DetailDisease.id_disease = Disease.id_disease;
                DetailDisease.id_pantient = IDTemp;
                dbModel.tbl_detail_disease.Add(DetailDisease);
                tbl_audit_trail audit = new tbl_audit_trail
                {
                    table_name = "tbl_detail_disease",
                    operation = "ADD",
                    occured_at = DateTime.Now,
                    performed_by = Session["FirstName"].ToString() + " " + Session["LastName"],
                    description = "Add select disease"
                };
                dbModel.tbl_audit_trail.Add(audit);
                dbModel.SaveChanges();
                dbModel.SaveChanges();
                List<tbl_detail_disease> DetailDiseaseList = dbModel.tbl_detail_disease.Where(s => s.id_pantient == IDTemp).ToList<tbl_detail_disease>();
                List<tbl_disease> DiseaseList = dbModel.tbl_disease.ToList<tbl_disease>();
                int MaxEnergy = 2700;
                foreach (var item in DetailDiseaseList)
                {
                    foreach (var item2 in DiseaseList)
                    {
                        if (item.id_disease == item2.id_disease)
                        {
                            if (item2.energy<=MaxEnergy)
                            {
                                MaxEnergy = Convert.ToInt32(item2.energy);
                            }
                        }
                    }
                }
                dbModel.Configuration.ValidateOnSaveEnabled = false;
                search.total_max_energy = MaxEnergy;
                dbModel.Entry(search).State = EntityState.Modified;
                dbModel.SaveChanges();
                
                return Json(new { success = true, message = "Disease Select Successfully" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(new { success = false, message = "Disease Select Failed" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {

            tbl_detail_disease disease = dbModel.tbl_detail_disease.Where(x => x.id_detail_disease == id).FirstOrDefault<tbl_detail_disease>();

            int IDTemp = Convert.ToInt32(Session["IdPatient"]);
            //update data meal total energy
            var search = dbModel.tbl_patient.Where(a => a.id_pantient == IDTemp).FirstOrDefault();
            if (search != null)
            {
                
                dbModel.tbl_detail_disease.Remove(disease);
                tbl_audit_trail audit = new tbl_audit_trail
                {
                    table_name = "tbl_detail_disease",
                    operation = "DELETE",
                    occured_at = DateTime.Now,
                    performed_by = Session["FirstName"].ToString() + " " + Session["LastName"],
                    description = "Delete select disease"
                };
                dbModel.tbl_audit_trail.Add(audit);
                dbModel.SaveChanges();
                List<tbl_detail_disease> DetailDiseaseList = dbModel.tbl_detail_disease.Where(s => s.id_pantient == IDTemp).ToList<tbl_detail_disease>();
                List<tbl_disease> DiseaseList = dbModel.tbl_disease.ToList<tbl_disease>();
                int MaxEnergy = 2700;
                foreach (var item in DetailDiseaseList)
                {
                    foreach (var item2 in DiseaseList)
                    {
                        if (item.id_disease == item2.id_disease)
                        {
                            if (item2.energy <= MaxEnergy)
                            {
                                MaxEnergy = Convert.ToInt32(item2.energy);
                            }
                        }
                    }
                }
                dbModel.Configuration.ValidateOnSaveEnabled = false;
                search.total_max_energy = MaxEnergy;
                dbModel.Entry(search).State = EntityState.Modified;
                dbModel.SaveChanges();

                return Json(new { success = true, message = "Delete Successfully" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(new { success = false, message = "Delete Failed" }, JsonRequestBehavior.AllowGet);
            }

        }

        public class DiseaseOutput
        {
            public int id_disease { get; set; }
            public string name_disease { get; set; }
            public int energy { get; set; }
        }

        public class SelectDiseaseOutput
        {
            public int id_disease { get; set; }
            public int id_detail_disease { get; set; }
            public string name_disease { get; set; }
            public int energy { get; set; }
        }

    }
}