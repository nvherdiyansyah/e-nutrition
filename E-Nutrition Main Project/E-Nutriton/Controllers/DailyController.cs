﻿using E_Nutriton.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Nutriton.Controllers
{
    public class DailyController : Controller
    {
        db_enutritionEntities dbModel = new db_enutritionEntities();
        // GET: Daily
        public ActionResult Index()
        {
            Session["IdDaily"] = null;
            return View();
        }

        [HttpGet]
        public JsonResult GetData()
        {
            using (db_enutritionEntities dbModel = new db_enutritionEntities())
            {
                //List<tbl_disease> diseaseList = dbModel.tbl_disease.ToList<tbl_disease>();
                //return Json(new { data = diseaseList }, JsonRequestBehavior.AllowGet);
                int IDPatient = Convert.ToInt32(Session["IdPatient"]);
                List<tbl_daily> dailyList = dbModel.tbl_daily.Where(a => a.id_pantient == IDPatient).ToList<tbl_daily>();
                List<DailyOutput> result = new List<DailyOutput>();
                foreach (var item in dailyList)
                {

                    result.Add(new DailyOutput
                    {
                        id_daily = item.id_daily,
                        id_hashing = item.id_hashing,
                        id_pantient = Convert.ToInt32(item.id_pantient),
                        weight_daily = Convert.ToInt32(item.weight_daily),
                        total_energy_daily = Convert.ToInt32(item.total_energy_daily),
                        complaints_disease = item.complaints_disease,
                        description = item.description,
                        daily_date = item.daily_date.ToString().Remove(10)
                    });
                }

                return Json(new { data = result }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        //public ActionResult AddorEdit(int id = 0)
        public ActionResult AddorEdit(string id = null)
        {
            if (id == null)
                return View(new tbl_daily());
            else
            {
                using (db_enutritionEntities dbModel = new db_enutritionEntities())
                {
                    string idhasing = id;
                    return View(dbModel.tbl_daily.Where(x => x.id_hashing == idhasing).FirstOrDefault<tbl_daily>());
                }
            }
        }

        [HttpPost]
        public ActionResult AddorEdit(tbl_daily daily)
        {
            using (var transaction = dbModel.Database.BeginTransaction())
            {
                try
                {
                    if (daily.id_hashing == null)
                    {
                        string id_hash = Guid.NewGuid().ToString().Remove(8);
                        int IDpasien = Convert.ToInt32(Session["IdPatient"]);
                        string datenow = DateTime.Now.ToShortDateString();
                        /*
                        //var searchdaily2 = dbModel.tbl_daily.Where(a => a.id_pantient == IDpasien).FirstOrDefault();
                        //string cek = searchdaily2.daily_date.ToString().Remove(10);
                        //var cek2 = searchdaily2.daily_date;
                        var searchdaily = dbModel.tbl_daily.Where(a => a.id_pantient == IDpasien && a.daily_date.ToString().Remove(10) == datenow).FirstOrDefault();
                        if (searchdaily != null)
                        {
                            return Json(new { success = false, message = "Saved Failed" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            daily.daily_date = DateTime.Now;
                            daily.id_hashing = id_hash;
                            dbModel.tbl_daily.Add(daily);
                            dbModel.SaveChanges();
                            return Json(new { success = true, message = "Saved Successfully" }, JsonRequestBehavior.AllowGet);
                        }
                        */
                        daily.daily_date = DateTime.Now;
                        daily.id_hashing = id_hash;
                        tbl_audit_trail audit = new tbl_audit_trail
                        {
                            table_name = "tbl_daily",
                            operation = "ADD",
                            occured_at = DateTime.Now,
                            performed_by = Session["FirstName"].ToString() + " " + Session["LastName"],
                            description = "Add Daily"
                        };
                        dbModel.tbl_audit_trail.Add(audit);
                        dbModel.tbl_daily.Add(daily);
                        dbModel.SaveChanges();
                        transaction.Commit();
                        return Json(new { success = true, message = "Saved Successfully" }, JsonRequestBehavior.AllowGet);

                    }
                    else
                    {
                        dbModel.Entry(daily).State = EntityState.Modified;
                        tbl_audit_trail audit = new tbl_audit_trail
                        {
                            table_name = "tbl_daily",
                            operation = "EDIT",
                            occured_at = DateTime.Now,
                            performed_by = Session["FirstName"].ToString() + " " + Session["LastName"],
                            description = "Edit Daily"
                        };
                        dbModel.tbl_audit_trail.Add(audit);
                        dbModel.SaveChanges();
                        transaction.Commit();
                        return Json(new { success = true, message = "Update Successfully" }, JsonRequestBehavior.AllowGet);

                    }
                }
                catch (Exception msgError)
                {
                    db_enutritionEntities dbModel2 = new db_enutritionEntities();

                    tbl_error_log errlog = new tbl_error_log()
                    {
                        error_line = msgError.StackTrace,
                        error_time = DateTime.Now,
                        performed_by = Session["FirstName"].ToString() + " " + Session["LastName"],
                        error_description = msgError.Message
                    };
                    dbModel2.tbl_error_log.Add(errlog);
                    dbModel2.SaveChanges();
                    transaction.Rollback();
                    return Json(new { success = true, message = "Error " + msgError.Message }, JsonRequestBehavior.AllowGet);
                } 
            }
        }
        
        
        
        [HttpGet]
        public ActionResult AddMeal(string id)
        {
            Session["IdDaily"] = id;
            Session["IdMeal"] = null;
            var search = dbModel.tbl_daily.Where(a => a.id_hashing == id).FirstOrDefault();

            return View(dbModel.tbl_meal.Where(b => b.id_daily == search.id_daily).ToList());
        }        

        [HttpPost]
        public ActionResult Edit(int id, tbl_daily Daily)
        {
            dbModel.Entry(Daily).State = EntityState.Modified;
            dbModel.SaveChanges();
            return View();
        }

        [HttpGet]
        public ActionResult Advice(string id)
        {
            int IDPatient = Convert.ToInt32(Session["IdPatient"]);
            var searchUser = dbModel.tbl_patient.Where(c => c.id_pantient == IDPatient).FirstOrDefault();
            var search = dbModel.tbl_daily.Where(a => a.id_hashing == id).FirstOrDefault();
            var searchadvice = dbModel.tbl_advice.Where(b => b.id_nutritionist == searchUser.id_nutritionist && b.id_daily == search.id_daily).FirstOrDefault();

            return View(searchadvice);
        }

        public class DailyOutput
        {
            public int id_daily { get; set; }
            public string id_hashing { get; set; }
            public int id_pantient { get; set; }
            public int weight_daily { get; set; }
            public int total_energy_daily { get; set; }
            public string complaints_disease { get; set; }
            public string daily_date { get; set; }
            public string description { get; set; }
        }

        
    }
}