﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using E_Nutriton.Models;

namespace E_Nutriton.Controllers
{
    public class PatientController : Controller
    {
        //Registration account
        db_enutritionEntities dbModel = new db_enutritionEntities();
        [HttpGet]
        public ActionResult Registration()
        {
            return View();
        }
        //Registration Post Action
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registration([Bind(Exclude = "is_email_valid,activation_code,is_account_active")] tbl_patient PatientModel)
        {
            using (var transaction = dbModel.Database.BeginTransaction())
            {
                try
                {
                    bool Status = false;
                    string Message = "";
                    // Model Validation
                    if (ModelState.IsValid)
                    {
                        #region // Email is already exist 
                        //model validation
                        var isExist = IsEmailExist(PatientModel.email);
                        if (isExist)
                        {
                            ModelState.AddModelError("EmailExist", "Email already exist");
                            return View();
                        }
                        #endregion


                        #region  // Generate Activation Code
                        PatientModel.activation_code = Guid.NewGuid();
                        #endregion

                        #region // Password Hassing
                        PatientModel.password = Crypto.Hash(PatientModel.password);
                        PatientModel.confirm_password = Crypto.Hash(PatientModel.confirm_password);
                        #endregion


                        PatientModel.is_email_valid = false;
                        PatientModel.picture = "~/img/picture/default_picture.jpg";
                        PatientModel.certificate = "~/img/certificate/default-certificate.jpg";
                        PatientModel.address = "";
                        PatientModel.symptoms = "";
                        PatientModel.total_max_energy = 2700;
                        string id_hash = Guid.NewGuid().ToString().Remove(8);
                        PatientModel.id_hashing = id_hash;
                        PatientModel.is_account_active = false;

                        #region // Save to database
                        dbModel.tbl_patient.Add(PatientModel);
                        dbModel.SaveChanges();
                        #endregion

                        // Send Email to User Nutristionist
                        SendVerificationLinkEmail(PatientModel.email, PatientModel.activation_code.ToString());
                        Message = "Registration successfully done. Account activation link " +
                            "has been seen to your email : " + PatientModel.email;
                        Status = true;
                    }
                    else
                    {
                        Message = "Invalid Request";
                        ViewBag.Message = Message;
                        ViewBag.Status = Status;
                        transaction.Commit();
                        return View();
                    }

                    ViewBag.Message = Message;
                    ViewBag.Status = Status;
                    TempData["Message"] = Message;
                    TempData["Status"] = Status;
                    transaction.Commit();
                    return RedirectToAction("Verifemail");
                }
                catch (Exception msgError)
                {

                    db_enutritionEntities dbModel2 = new db_enutritionEntities();

                    tbl_error_log errlog = new tbl_error_log()
                    {
                        error_line = msgError.StackTrace,
                        error_time = DateTime.Now,
                        error_description = msgError.Message
                    };
                    dbModel2.tbl_error_log.Add(errlog);
                    dbModel2.SaveChanges();

                    ViewBag.Message = msgError.Message;
                    ViewBag.Status = false;
                    transaction.Rollback();
                    return View();
                } 
            }
        }



        //untuk alihkan halaman regis
        public ActionResult Verifemail()
        {
            ViewBag.Message = TempData["Message"];
            ViewBag.Status = TempData["Status"];
            return View();
        }



        //verify Account
        [HttpGet]
        public ActionResult VerifyAccount(string id)
        {
            bool Status = false;
            dbModel.Configuration.ValidateOnSaveEnabled = false; // this line to avoid confirm password does not
                                                                 // match issue on save change
            var v = dbModel.tbl_patient.Where(a => a.activation_code == new Guid(id)).FirstOrDefault();
            if (v != null)
            {
                v.is_email_valid = true;
                dbModel.SaveChanges();
                Status = true;
            }
            else
            {
                ViewBag.Message = "Invalid Request";
            }

            ViewBag.Status = Status;
            return View();
        }

        //Verify Email Link
        
            //Login
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        //Login Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(patientLogin login, string ReturnUrl)
        {
            using (var transaction = dbModel.Database.BeginTransaction())
            {
                try
                {
                    string message = "";
                    var v = dbModel.tbl_patient.Where(a => a.email == login.email).FirstOrDefault();
                    if (v != null)
                    {
                        if (string.Compare(Crypto.Hash(login.password), v.password) == 0)
                        {
                            if (v.is_email_valid == true)
                            {
                                // make time out for cookie login with Remember Me
                                int timeout = login.rememberme ? 525600 : 20; // 525600 minutes is 1 year
                                var ticket = new FormsAuthenticationTicket(login.email, login.rememberme, timeout);
                                string encrypted = FormsAuthentication.Encrypt(ticket);
                                var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted);
                                cookie.Expires = DateTime.Now.AddMinutes(timeout);
                                cookie.HttpOnly = true;
                                Response.Cookies.Add(cookie);

                                if (Url.IsLocalUrl(ReturnUrl))
                                {
                                    return Redirect(ReturnUrl);
                                }
                                else
                                {
                                    ViewBag.IsAccoutActive = v.is_account_active;
                                    Session["LoginPatient"] = "Login";
                                    Session["IdPatient"] = v.id_pantient.ToString();
                                    Session["IsAccoutActive"] = v.is_account_active;
                                    Session["FirstName"] = v.first_name.ToString();
                                    Session["LastName"] = v.last_name.ToString();
                                    Session["Picture"] = v.picture.ToString();


                                    return RedirectToAction("Index", "DashboardPatient");
                                }
                            }
                            else
                            {
                                message = "Your email is not active, please activate your email";
                            }
                        }
                        else
                        {
                            message = "Your password is incorrect";
                        }
                    }
                    else
                    {
                        message = "Your email is wrong or not registered ";
                    }

                    ViewBag.Message = message;
                    transaction.Commit();
                    return View();
                }
                catch (Exception msgError)
                {
                    db_enutritionEntities dbModel2 = new db_enutritionEntities();

                    tbl_error_log errlog = new tbl_error_log()
                    {
                        error_line = msgError.StackTrace,
                        error_time = DateTime.Now,
                        error_description = msgError.Message
                    };
                    dbModel2.tbl_error_log.Add(errlog);
                    dbModel2.SaveChanges();

                    ViewBag.Message = msgError.Message;
                    ViewBag.Status = false;
                    transaction.Rollback();
                    return View();
                } 
            }
        }
        //Login Action


        //Logout
        [Authorize]
        [HttpPost]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            Session.Abandon();
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            return RedirectToAction("Login", "Patient");
        }


        [NonAction]
        public bool IsEmailExist(string Email)
        {
            var v = dbModel.tbl_patient.Where(a => a.email == Email).FirstOrDefault();
            return v != null;
        }

        [NonAction]
        public void SendVerificationLinkEmail(string Email, string ActivationCode, string emailfor = "VerifyAccount")
        {

            var verifyUrl = "/Patient/" + emailfor + "/" + ActivationCode;
            var checkUrl = Request.Url.AbsoluteUri;
            if (checkUrl.Contains("/E-Nutrition/"))
            {
                verifyUrl = "/E-Nutrition/Patient/" + emailfor + "/" + ActivationCode;
            }

            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);

            var fromEmail = new MailAddress("project.enutrition@gmail.com", "E-Nutrition");
            var toEmail = new MailAddress(Email);
            var fromEmailPassword = "nawaB7sln"; // Replace with actual password

            string subject = "";
            string body = "";
            if (emailfor == "VerifyAccount")
            {
                subject = "Your E-Nutrition account is successfully created!";
                body = "<br/><br/>We are excited to tell you that your E-Nutrition account is " +
                    "successfully created. Please click on the below link to verify your account" +
                    "<br/><br/><a href='" + link + "'>" + link + "</a>";
            }
            else if (emailfor == "ResetPassword")
            {
                subject = "Reset Password";
                body = "<br/><br/>We got request for reset your account password. Please click on below link to reset your password " +
                    "<br/><br/><a href='" + link + "'>Reset Password</a>";
            }

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };

            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })

                smtp.Send(message);


        }

    }

}