﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Nutriton.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        //[Route("dashboard")]
        public ActionResult Dashboard()
        {
            return View();
        }
        public ActionResult Coba()
        {
            return View();
        }
    }
}
