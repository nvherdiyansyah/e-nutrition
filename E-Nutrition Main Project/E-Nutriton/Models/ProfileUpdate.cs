﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace E_Nutriton.Models
{
    public class ProfileUpdate
    {
        public int id_nutritionist { get; set; }

        [Display(Name = "First Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "First name is required")]
        public string first_name { get; set; }

        [Display(Name = "Last Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Last name is required")]
        public string last_name { get; set; }

        [Display(Name = "Gender")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Gender is required")]
        public string gender { get; set; }

        [Display(Name = "Address")]
        public string address { get; set; }

        [Display(Name = "Date of Birth")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime date_birth { get; set; }

        public string picture { get; set; }

        public HttpPostedFileBase ImagePictureFile { get; set; }

        public string certificate { get; set; }

        public HttpPostedFileBase ImageCertificateFile { get; set; }

    }
}