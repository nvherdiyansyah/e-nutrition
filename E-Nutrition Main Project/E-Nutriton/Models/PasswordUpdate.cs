﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace E_Nutriton.Models
{
    public class PasswordUpdate
    {
        public int id_nutritionist { get; set; }

        [Display(Name = "Current Password")]
        [Required(ErrorMessage = "Current Password required", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        public string current_password { get; set; }

        [Display(Name = "New Password")]
        [Required(ErrorMessage = "New Password required", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        public string new_password { get; set; }

        [Display(Name = "Confrim Password")]
        [DataType(DataType.Password)]
        [Compare("new_password", ErrorMessage = "New password and confirm password does not match")]
        public string confirm_password { get; set; }
        
    }
}