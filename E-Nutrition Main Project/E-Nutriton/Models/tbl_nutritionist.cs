//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace E_Nutriton.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_nutritionist
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_nutritionist()
        {
            this.tbl_advice = new HashSet<tbl_advice>();
            this.tbl_patient = new HashSet<tbl_patient>();
        }
    
        public int id_nutritionist { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string picture { get; set; }
        public Nullable<System.DateTime> date_birth { get; set; }
        public string certificate { get; set; }
        public Nullable<bool> is_email_valid { get; set; }
        public Nullable<System.Guid> activation_code { get; set; }
        public string gender { get; set; }
        public string address { get; set; }
        public Nullable<bool> is_account_active { get; set; }
        public string id_hashing { get; set; }
        public string reset_password_code { get; set; }
        public string description { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_advice> tbl_advice { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_patient> tbl_patient { get; set; }
    }
}
