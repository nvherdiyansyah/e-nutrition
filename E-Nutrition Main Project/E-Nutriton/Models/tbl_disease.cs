//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace E_Nutriton.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_disease
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_disease()
        {
            this.tbl_detail_disease = new HashSet<tbl_detail_disease>();
        }
    
        public int id_disease { get; set; }
        public string name_disease { get; set; }
        public Nullable<int> energy { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_detail_disease> tbl_detail_disease { get; set; }
    }
}
