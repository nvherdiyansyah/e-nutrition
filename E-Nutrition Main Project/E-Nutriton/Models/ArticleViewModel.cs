﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Nutriton.Models
{
    public class ArticleViewModel
    {
        public int Id_article { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public Nullable<System.DateTime> Release_date { get; set; }
        [AllowHtml]
        [Required]
        public string Content { get; set; }
        [Required]
        public byte[] Picture { get; set; }
    }
}