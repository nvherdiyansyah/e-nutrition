﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Nutriton.Models
{
    public class UserUpdateView
    {
        public ProfileUpdate ProfileUpdate { get; set; }
        public PasswordUpdate PasswordUpdate { get; set; }
    }
}