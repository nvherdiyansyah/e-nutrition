﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;

namespace E_Nutriton.Models
{
    public class ArticleRepositories
    {
        private readonly db_enutritionEntities dbModel = new db_enutritionEntities();
        public int UploadImageInDataBase(HttpPostedFileBase file, ArticleViewModel contentViewModel)
        {
            contentViewModel.Picture = ConvertToBytes(file);
            var Content = new tbl_article
            {
                Title = contentViewModel.Title,
                Content = contentViewModel.Content,
                Release_date = DateTime.Today,
                Picture = contentViewModel.Picture
            };

            dbModel.tbl_article.Add(Content);
            int i = dbModel.SaveChanges();
            if (i == 1)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public int UpdateImageInDataBase(HttpPostedFileBase file, ArticleViewModel contentViewModel)
        {
            contentViewModel.Picture = ConvertToBytes(file);
            var Content = new tbl_article
            {
                Title = contentViewModel.Title,
                Content = contentViewModel.Content,
                Release_date = DateTime.Today,
                Picture = contentViewModel.Picture
            };

            dbModel.Entry(Content).State=EntityState.Modified;
            int i = dbModel.SaveChanges();
            if (i == 1)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }
        public byte[] ConvertToBytes(HttpPostedFileBase image)
        {
            byte[] imageBytes = null;
            BinaryReader reader = new BinaryReader(image.InputStream);
            imageBytes = reader.ReadBytes((int)image.ContentLength);
            return imageBytes;
        }
    }
}
