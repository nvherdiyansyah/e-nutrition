﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace E_Nutriton.Models
{
    public class ResetPasswordModel
    {
        [Required(ErrorMessage ="New Password required", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        public string new_password { get; set; }

        [DataType(DataType.Password)]
        [Compare("new_password", ErrorMessage = "New password and confirm password does not match")]
        public string confirm_password { get; set; }

        [Required]
        public string reset_code { get; set; }
    }
}