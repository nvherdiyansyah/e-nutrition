﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_Nutriton.Models
{
    [MetadataType(typeof(DetailFoodMetadata))]
    public partial class tbl_detail_food
    {
        [NotMapped]
        public List<tbl_food> FoodCollection { get; set; }

        [NotMapped]
        public List<tbl_meal> MealCollection { get; set; }
    }

    public class DetailFoodMetadata
    {
        public int id_detail_food { get; set; }
        public int id_meal { get; set; }
        public int id_food { get; set; }

    }
}