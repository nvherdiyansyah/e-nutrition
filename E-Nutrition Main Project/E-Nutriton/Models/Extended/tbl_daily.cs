﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_Nutriton.Models
{
    [MetadataType(typeof(DailyMetadata))]
    public partial class tbl_daily
    {
        [NotMapped]
        public List<tbl_meal> mealCollection { get; set; }

        [NotMapped]
        public List<tbl_patient> patintCollection { get; set; }

        [NotMapped]
        public List<tbl_advice> adviceCollection { get; set; }
    }

    public class DailyMetadata
    {
        public int id_daily { get; set; }

        public int id_pantient { get; set; }

        public int weight_daily { get; set; }

        public int total_energy_daily { get; set; }

        public string complaints_disease { get; set; }
        
        public string id_hashing { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime daily_date { get; set; }
        
    }
}