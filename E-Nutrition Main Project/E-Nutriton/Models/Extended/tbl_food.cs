﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_Nutriton.Models
{
    [MetadataType(typeof(FoodMetadata))]
    public partial class tbl_food
    {
        [NotMapped]
        public List<tbl_detail_food> detailCollection { get; set; }
    }

    public class FoodMetadata
    {
        public int id_food { get; set; }

        [Display(Name = "Food Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Food Name is required")]
        public string name_food { get; set; }

        [Display(Name = "Gram")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Gram is required")]
        public int portion { get; set; }

        [Display(Name = "Energy")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Energy is required")]
        public int energy { get; set; }

        [Display(Name = "Carbohydrate")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Carbohydrate is required")]
        public int carbohydrate { get; set; }

        [Display(Name = "Protein")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Protein is required")]
        public int protein { get; set; }

        [Display(Name = "Fat")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Fat is required")]
        public int fat { get; set; }

    }
}