﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_Nutriton.Models
{
    [MetadataType(typeof(DiseaseMetadata))]
    public partial class tbl_disease
    {
        [NotMapped]
        public List<tbl_detail_disease> detailDiseaseCollection { get; set; }
    }

    public class DiseaseMetadata
    {

        public int id_disease { get; set; }

        [Display(Name = "Disease Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Disease Name is required")]
        public string name_disease { get; set; }

        [Display(Name = "Energy")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Energy is required")]
        public int energy { get; set; }

    }
}