﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace E_Nutriton.Models
{
    [MetadataType(typeof(UserMetadata))]
    public partial class tbl_nutritionist
    {
        public HttpPostedFileBase ImagePictureFile { get; set; }
        public HttpPostedFileBase ImageCertificateFile { get; set; }
        public PasswordUpdate PasswordUpdate { get; set; }
        public string confirm_password { get; set; }
    }

    public class UserMetadata
    {
        [Display(Name = "First Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "First name is required")]
        public string first_name { get; set; }

        [Display(Name = "Last Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Last name is required")]
        public string last_name { get; set; }

        [Display(Name = "Email")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email is requiared")]
        [DataType(DataType.EmailAddress)]
        public string email { get; set; }

        [Display(Name = "Password")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Password is required")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Minimum 6 character required")]
        public string password { get; set; }

        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        [Compare("password", ErrorMessage = "Password and Confirm Password do not match")]
        public string confirm_password { get; set; }

        [Display(Name = "Gender")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Gender is required")]
        public string gender { get; set; }

        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime date_birth { get; set; }

        [Display(Name = "Address")]
        public string address { get; set; }

        [Display(Name = "Description")]
        public string description { get; set; }

        public string picture { get; set; }

        public HttpPostedFileBase ImagePictureFile { get; set; }

        public string certificate { get; set; }

        public HttpPostedFileBase ImageCertificateFile { get; set; }

        public PasswordUpdate PasswordUpdate { get; set; }
    }
}