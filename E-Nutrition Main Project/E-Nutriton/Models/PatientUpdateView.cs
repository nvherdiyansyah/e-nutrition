﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Nutriton.Models
{
    public class PatientUpdateView
    {
        public ProfileUpdatePatient ProfileUpdatePatient { get; set; }
        public tbl_patient Tbl_Patient { get; set; }
        public PasswordUpdatePatient PasswordUpdatePatient { get; set; }
        public tbl_nutritionist  selectnutritionist { get; set; }
    }
}