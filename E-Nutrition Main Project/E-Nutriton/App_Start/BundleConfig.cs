﻿using System.Web;
using System.Web.Optimization;

namespace E_Nutriton
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                       "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                       //untuk pagination
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/jquery-3.3.1.js",
                      "~/Scripts/jquery-3.1.1.min.js",
                      "~/Scripts/jquery.validate.min.js",
                      "~/Scripts/jquery.validate.unobtrusive.min.js",
                      "~/Scripts/jquery-ui-1.12.1.min.js"));

            bundles.Add(new StyleBundle("~/Contenting/css").Include(
                      "~/Content/demo.css",
                      "~/Content/main.css"));

            bundles.Add(new StyleBundle("~/Vendoring/css").Include(
                      "~/vendor/bootstrap/css/bootstrap.min.css",
                      "~/vendor/font-awesome/css/font-awesome.min.css",
                      "~/vendor/linearicons/style.css",
                      "~/vendor/metisMenu/metisMenu.css",
                      "~/vendor/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css",
                      "~/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css",
                      "~/vendor/chartist/css/chartist.min.css",
                      "~/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css",
                      "~/vendor/toastr/toastr.min.css",
                      "~/vendor/dropify/css/dropify.min.css"));

            bundles.Add(new ScriptBundle("~/Scripting/js").Include(
                      "~/Scripts/common.js"));

            bundles.Add(new ScriptBundle("~/Vendoring/js").Include(
                      "~/vendor/jquery/jquery.min.js",
                      "~/vendor/bootstrap/js/bootstrap.min.js",
                      "~/vendor/metisMenu/metisMenu.js",
                      "~/vendor/jquery-slimscroll/jquery.slimscroll.min.js",
                      "~/vendor/jquery-sparkline/js/jquery.sparkline.min.js",
                      "~/vendor/bootstrap-progressbar/js/bootstrap-progressbar.min.js",
                      "~/vendor/chartist/js/chartist.min.js",
                      "~/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.min.js",
                      "~/vendor/chartist-plugin-axistitle/chartist-plugin-axistitle.min.js",
                      "~/vendor/chartist-plugin-legend-latest/chartist-plugin-legend.js",
                      "~/vendor/toastr/toastr.js",
                      "~/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js",
                      "~/vendor/dropify/js/dropify.min.js"));



            // Untuk tampilan master data
            bundles.Add(new ScriptBundle("~/VendoringMaster/js").Include(
                      "~/vendor/jquery/jquery.min.js",
                      "~/vendor/bootstrap/js/bootstrap.min.js",
                      "~/vendor/metisMenu/metisMenu.js",
                      "~/vendor/jquery-slimscroll/jquery.slimscroll.min.js"));

            bundles.Add(new ScriptBundle("~/ScriptingMaster/js").Include(
                      "~/Scripts/common.js"));

            bundles.Add(new StyleBundle("~/VendoringMaster/css").Include(
                      "~/vendor/bootstrap/css/bootstrap.min.css",
                      "~/vendor/font-awesome/css/font-awesome.min.css",
                      "~/vendor/linearicons/style.css",
                      "~/vendor/metisMenu/metisMenu.css"));

            bundles.Add(new StyleBundle("~/ContentingMaster/css").Include(
                      "~/Content/demo.css",
                      "~/Content/main.css"));


            //untuk halaman awal
            bundles.Add(new StyleBundle("~/Contenting1/css").Include(
                     "~/Content/swipe.css"
                  ));

            bundles.Add(new StyleBundle("~/Vendoring1/css").Include(
                      "~/vendormain/@fortawesome/fontawesome-free/css/all.min.css"
                   ));
            bundles.Add(new StyleBundle("~/Vendoring1/js").Include(
                       "~/vendormain/popper.js/dist/umd/popper.min.js",
                     "~/vendormain/bootstrap/dist/js/bootstrap.min.js",
                     "~/vendormain/headroom.js/dist/headroom.min.js",
                     "~/vendormain/smooth-scroll/dist/smooth-scroll.polyfills.min.js",
                     "~/vendormain/owl.carousel/owl.carousel.min.js",
                     "~/vendormain/venobox/venobox.min.js",
                     "~/vendormain/aos/aos.js"
                  ));


        }
    }
}
