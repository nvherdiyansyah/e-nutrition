
Akun Admin
username : superadmin
password : admin123

Akun Ahli Gizi
email : novan.herdiyansyah@gmail.com
password : aweh123

Akun Pasien
email : novan.herdiyansyah@gmail.com
password : aweh123

-----------------------------------------------------

LINK Login Admin          : localhost/admin
LINK Login User Pasien    : localhost/patient/login
LINK Login User Ahli Gizi : localhost/user/login


----------------------------------------------------
     Panduan Instalasi Aplikasi E-Nutrition

==============================================
		   Installasi 
==============================================
- Pull semua file dari repository Master E-Nutrition di
  GitLab dan tempatkan pada satu file.
-Jika sudah,buka visual studio lalu Generate  database schema pada model edmx 
-Jika sudah Buka ssms,lalu  execute script db_enutrition_data_only.sql
  yang terdapat dalam folder repository tersebut
- Untuk View bisa di litah di view_catatan.txt 
-Lalu setelah  itu update model edmx 
-Jika sudah Build Projek tersebut 

==============================================
		   Home Menu
==============================================
- Halaman Utama dari aplikasi E- nutrition 
- Terdapat Tombol Register (digunakan untuk user yang belum memiliki akun)
- Terdapat Tombol Login (digunakan untuk user yang sudah memiliki akun dan ingin menggunakan aplikai ini)
-Terdapat informasi yang berisi tentang aplikasi E-Nutrition 
- Footer yang berisi informasi kontak dibagian
  contact us. 
==============================================
		   Masuk Ke Halaman Pasien 
==============================================
- Jika User Belum Memiliki Akun harap melakukan registrasi terlebih dahulu
- Jika user sudah memiliki Akun user dapat menggunakan aplikasi E- Nutrition ini
- Sebelum user dapat menggunakan Aplikasi ini User wajib untuk merubah data diri pada 
halaman Dashboard  klik pojok kanan terdapat Ikon user lalu pilih My Profile 
- Pada halaman My Profile User di wajibkan mengisi data diri berupa , Bb,Tb, Keluhan , alamat , jenis kelamin 
- setelah itu User wajib mengupload Surat Keterangan Covid ,
- Jika sudah mengisi data diri  pilih tombol update ,
-Lalu selanjutnya  masih di halaman My Profile User wajib memilih Ahli gizi degan memilih Tombol Select 
- Jika User memiliki Riwayat Penyakit user dapat mengisikan riwayat penyakit nya  dengan memilih tombol Disease
-Jika data diri sudah di perbarui user harap menunggu , karna data tersebut akan di verifikasi oleh Ahli Gizi 
- Jika Ahli gizi sudah memverifikasi , User dapat menggunakan aplikasi tersebut 
- Selanjutnya untuk mengisi asupan makanana , user dapat memilih menu Daily , 
dalam halaman ini user wajib menginputkan berat badan harian , keluhan serta asupan makanan yang di makan pada hari itu
terdapat 3 kategori yaitu Breakfast, Dinner, Lunch 
- Setelah User menginputkan asupaan makanan , ahli gizi akan melihat record dari asupan pasien tersebut,
- tunggu beberapa saat ahli gizi akan memberikan saran dan masukan terhadap apa yang  user makan hari ini 
- Pada halaman dashboard ini terdapat Artikel yang berisi informasi kesehatan 
==============================================
		   Masuk Ke Halaman Ahli Gizi 
==============================================
- Jika User Belum Memiliki Akun harap melakukan registrasi terlebih dahulu
- Jika user sudah memiliki Akun user dapat menggunakan aplikasi E- Nutrition ini
- Sebelum user dapat menggunakan Aplikasi ini User wajib untuk merubah data diri pada 
  halaman Dashboard  klik pojok kanan terdapat Ikon user lalu pilih My Profile 
- Pada halaman My Profile User di wajibkan mengisi data diri 
- setelah itu User wajib mengupload Surat Sertifikat Ahli gizi ,
- Jika sudah mengisi data diri  pilih tombol update ,
- Tunggu beberapa saat akun Ahligizi ini nanti nya akan di Verifikasi Oleh Admin
-Setelah di verivikasi oleh admin , nutritionist dapat menggunakan aplikasi ini , 
-Setelah itu pada menu list registrattion pasien terdapat data asien yang memilih ahligizi tersebut untuk menjadi ahligizi nya 
- langkah selanjut nya ahligizi di haruskan mengaprrove pasien tersebut  (Harap di cek Surat keterangan Covid nya)
-Setelah itu Ahli gizi dapat mengontrol asupan gizi pasien dengan memilih menu list pasien 
  dan pilih tombol select yang nanti nya akan muncul record harian si pasien tersebut 
- jika sudah dilihat ahli gizi dapat memberikan saran terhadap pasien tersebut dengan memilih tombol advice 
==============================================
		   Masuk Ke Halaman Admin
==============================================
-Di tampilkan halaman yang berisi record daripasien, dan ahli gizi 
-Terdapat beberapa master (Food , Disease, Article)
-Terdapat List Report yang berisi report dari menu master , pasien, ahligizi dan  record user



